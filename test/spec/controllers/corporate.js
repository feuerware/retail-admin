'use strict';

describe('Controller: CorporateCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var CorporateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CorporateCtrl = $controller('CorporateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
