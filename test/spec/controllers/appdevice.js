'use strict';

describe('Controller: AppdeviceCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var AppdeviceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AppdeviceCtrl = $controller('AppdeviceCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
