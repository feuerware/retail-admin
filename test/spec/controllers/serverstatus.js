'use strict';

describe('Controller: ServerstatusCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var ServerstatusCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ServerstatusCtrl = $controller('ServerstatusCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
