'use strict';

describe('Controller: MailsCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var MailsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MailsCtrl = $controller('MailsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
