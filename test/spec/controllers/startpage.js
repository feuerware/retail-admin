'use strict';

describe('Controller: StartpageCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var StartpageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StartpageCtrl = $controller('StartpageCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
