'use strict';

describe('Controller: KeeperCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var KeeperCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    KeeperCtrl = $controller('KeeperCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
