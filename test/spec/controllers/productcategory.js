'use strict';

describe('Controller: ProductcategoryCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var ProductcategoryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductcategoryCtrl = $controller('ProductcategoryCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
