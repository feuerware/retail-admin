'use strict';

describe('Controller: StatisticsuserCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var StatisticsuserCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StatisticsuserCtrl = $controller('StatisticsuserCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
