'use strict';

describe('Controller: QrcodeCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var QrcodeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    QrcodeCtrl = $controller('QrcodeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
