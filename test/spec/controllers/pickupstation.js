'use strict';

describe('Controller: PickupstationCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var PickupstationCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PickupstationCtrl = $controller('PickupstationCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
