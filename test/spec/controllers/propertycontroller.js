'use strict';

describe('Controller: PropertycontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var PropertycontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PropertycontrollerCtrl = $controller('PropertycontrollerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
