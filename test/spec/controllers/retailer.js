'use strict';

describe('Controller: RetailerCtrl', function () {

  // load the controller's module
  beforeEach(module('cubeAdminApp'));

  var RetailerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RetailerCtrl = $controller('RetailerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
