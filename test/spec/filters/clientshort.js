'use strict';

describe('Filter: clientShort', function () {

  // load the filter's module
  beforeEach(module('cubeAdminApp'));

  // initialize a new instance of the filter before each test
  var clientShort;
  beforeEach(inject(function ($filter) {
    clientShort = $filter('clientShort');
  }));

  it('should return the input prefixed with "clientShort filter:"', function () {
    var text = 'angularjs';
    expect(clientShort(text)).toBe('clientShort filter: ' + text);
  });

});
