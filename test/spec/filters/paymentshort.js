'use strict';

describe('Filter: paymentShort', function () {

  // load the filter's module
  beforeEach(module('cubeAdminApp'));

  // initialize a new instance of the filter before each test
  var paymentShort;
  beforeEach(inject(function ($filter) {
    paymentShort = $filter('paymentShort');
  }));

  it('should return the input prefixed with "paymentShort filter:"', function () {
    var text = 'angularjs';
    expect(paymentShort(text)).toBe('paymentShort filter: ' + text);
  });

});
