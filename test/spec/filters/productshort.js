'use strict';

describe('Filter: ProductShort', function () {

  // load the filter's module
  beforeEach(module('cubeAdminApp'));

  // initialize a new instance of the filter before each test
  var ProductShort;
  beforeEach(inject(function ($filter) {
    ProductShort = $filter('ProductShort');
  }));

  it('should return the input prefixed with "ProductShort filter:"', function () {
    var text = 'angularjs';
    expect(ProductShort(text)).toBe('ProductShort filter: ' + text);
  });

});
