'use strict';

describe('Filter: input', function () {

  // load the filter's module
  beforeEach(module('cubeAdminApp'));

  // initialize a new instance of the filter before each test
  var input;
  beforeEach(inject(function ($filter) {
    input = $filter('input');
  }));

  it('should return the input prefixed with "input filter:"', function () {
    var text = 'angularjs';
    expect(input(text)).toBe('input filter: ' + text);
  });

});
