'use strict';

describe('Filter: usershort', function () {

  // load the filter's module
  beforeEach(module('cubeAdminApp'));

  // initialize a new instance of the filter before each test
  var usershort;
  beforeEach(inject(function ($filter) {
    usershort = $filter('usershort');
  }));

  it('should return the input prefixed with "usershort filter:"', function () {
    var text = 'angularjs';
    expect(usershort(text)).toBe('usershort filter: ' + text);
  });

});
