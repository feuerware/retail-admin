'use strict';

describe('Filter: buildingAddressShort', function () {

  // load the filter's module
  beforeEach(module('cubeAdminApp'));

  // initialize a new instance of the filter before each test
  var buildingAddressShort;
  beforeEach(inject(function ($filter) {
    buildingAddressShort = $filter('buildingAddressShort');
  }));

  it('should return the input prefixed with "buildingAddressShort filter:"', function () {
    var text = 'angularjs';
    expect(buildingAddressShort(text)).toBe('buildingAddressShort filter: ' + text);
  });

});
