'use strict';

describe('Filter: pickupstationshort', function () {

  // load the filter's module
  beforeEach(module('cubeAdminApp'));

  // initialize a new instance of the filter before each test
  var pickupstationshort;
  beforeEach(inject(function ($filter) {
    pickupstationshort = $filter('pickupstationshort');
  }));

  it('should return the input prefixed with "pickupstationshort filter:"', function () {
    var text = 'angularjs';
    expect(pickupstationshort(text)).toBe('pickupstationshort filter: ' + text);
  });

});
