'use strict';

describe('Filter: designSettingsShort', function () {

  // load the filter's module
  beforeEach(module('cubeAdminApp'));

  // initialize a new instance of the filter before each test
  var designSettingsShort;
  beforeEach(inject(function ($filter) {
    designSettingsShort = $filter('designSettingsShort');
  }));

  it('should return the input prefixed with "designSettingsShort filter:"', function () {
    var text = 'angularjs';
    expect(designSettingsShort(text)).toBe('designSettingsShort filter: ' + text);
  });

});
