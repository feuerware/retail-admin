'use strict';

describe('Filter: QrCodeGenerator', function () {

  // load the filter's module
  beforeEach(module('cubeAdminApp'));

  // initialize a new instance of the filter before each test
  var QrCodeGenerator;
  beforeEach(inject(function ($filter) {
    QrCodeGenerator = $filter('QrCodeGenerator');
  }));

  it('should return the input prefixed with "QrCodeGenerator filter:"', function () {
    var text = 'angularjs';
    expect(QrCodeGenerator(text)).toBe('QrCodeGenerator filter: ' + text);
  });

});
