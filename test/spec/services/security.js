'use strict';

describe('Service: Security', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Security;
  beforeEach(inject(function (_Security_) {
    Security = _Security_;
  }));

  it('should do something', function () {
    expect(!!Security).toBe(true);
  });

});
