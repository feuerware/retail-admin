'use strict';

describe('Service: Simplegenerator', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Simplegenerator;
  beforeEach(inject(function (_Simplegenerator_) {
    Simplegenerator = _Simplegenerator_;
  }));

  it('should do something', function () {
    expect(!!Simplegenerator).toBe(true);
  });

});
