'use strict';

describe('Service: Importerstatus', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Importerstatus;
  beforeEach(inject(function (_Importerstatus_) {
    Importerstatus = _Importerstatus_;
  }));

  it('should do something', function () {
    expect(!!Importerstatus).toBe(true);
  });

});
