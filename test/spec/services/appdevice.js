'use strict';

describe('Service: Appdevice', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Appdevice;
  beforeEach(inject(function (_Appdevice_) {
    Appdevice = _Appdevice_;
  }));

  it('should do something', function () {
    expect(!!Appdevice).toBe(true);
  });

});
