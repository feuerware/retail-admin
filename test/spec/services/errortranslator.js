'use strict';

describe('Service: ErrorTranslator', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var ErrorTranslator;
  beforeEach(inject(function (_ErrorTranslator_) {
    ErrorTranslator = _ErrorTranslator_;
  }));

  it('should do something', function () {
    expect(!!ErrorTranslator).toBe(true);
  });

});
