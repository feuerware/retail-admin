'use strict';

describe('Service: Statistics', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Statistics;
  beforeEach(inject(function (_Statistics_) {
    Statistics = _Statistics_;
  }));

  it('should do something', function () {
    expect(!!Statistics).toBe(true);
  });

});
