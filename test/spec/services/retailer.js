'use strict';

describe('Service: Retailer', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Retailer;
  beforeEach(inject(function (_Retailer_) {
    Retailer = _Retailer_;
  }));

  it('should do something', function () {
    expect(!!Retailer).toBe(true);
  });

});
