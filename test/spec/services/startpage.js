'use strict';

describe('Service: Startpage', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Startpage;
  beforeEach(inject(function (_Startpage_) {
    Startpage = _Startpage_;
  }));

  it('should do something', function () {
    expect(!!Startpage).toBe(true);
  });

});
