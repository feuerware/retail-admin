'use strict';

describe('Service: propertyservice', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var propertyservice;
  beforeEach(inject(function (_propertyservice_) {
    propertyservice = _propertyservice_;
  }));

  it('should do something', function () {
    expect(!!propertyservice).toBe(true);
  });

});
