'use strict';

describe('Service: Productcategory', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Productcategory;
  beforeEach(inject(function (_Productcategory_) {
    Productcategory = _Productcategory_;
  }));

  it('should do something', function () {
    expect(!!Productcategory).toBe(true);
  });

});
