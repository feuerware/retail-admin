'use strict';

describe('Service: Qrcode', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Qrcode;
  beforeEach(inject(function (_Qrcode_) {
    Qrcode = _Qrcode_;
  }));

  it('should do something', function () {
    expect(!!Qrcode).toBe(true);
  });

});
