'use strict';

describe('Service: Mails', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Mails;
  beforeEach(inject(function (_Mails_) {
    Mails = _Mails_;
  }));

  it('should do something', function () {
    expect(!!Mails).toBe(true);
  });

});
