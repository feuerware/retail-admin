'use strict';

describe('Service: Notificationlive', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Notificationlive;
  beforeEach(inject(function (_Notificationlive_) {
    Notificationlive = _Notificationlive_;
  }));

  it('should do something', function () {
    expect(!!Notificationlive).toBe(true);
  });

});
