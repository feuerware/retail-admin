'use strict';

describe('Service: Mockpickupstation', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Mockpickupstation;
  beforeEach(inject(function (_Mockpickupstation_) {
    Mockpickupstation = _Mockpickupstation_;
  }));

  it('should do something', function () {
    expect(!!Mockpickupstation).toBe(true);
  });

});
