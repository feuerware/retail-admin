'use strict';

describe('Service: Livelog', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Livelog;
  beforeEach(inject(function (_Livelog_) {
    Livelog = _Livelog_;
  }));

  it('should do something', function () {
    expect(!!Livelog).toBe(true);
  });

});
