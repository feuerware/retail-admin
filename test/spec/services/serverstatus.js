'use strict';

describe('Service: Serverstatus', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Serverstatus;
  beforeEach(inject(function (_Serverstatus_) {
    Serverstatus = _Serverstatus_;
  }));

  it('should do something', function () {
    expect(!!Serverstatus).toBe(true);
  });

});
