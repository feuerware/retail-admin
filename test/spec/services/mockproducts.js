'use strict';

describe('Service: Mockproducts', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Mockproducts;
  beforeEach(inject(function (_Mockproducts_) {
    Mockproducts = _Mockproducts_;
  }));

  it('should do something', function () {
    expect(!!Mockproducts).toBe(true);
  });

});
