'use strict';

describe('Service: Properties', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Properties;
  beforeEach(inject(function (_Properties_) {
    Properties = _Properties_;
  }));

  it('should do something', function () {
    expect(!!Properties).toBe(true);
  });

});
