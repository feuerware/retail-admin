'use strict';

describe('Service: Pickupstation', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Pickupstation;
  beforeEach(inject(function (_Pickupstation_) {
    Pickupstation = _Pickupstation_;
  }));

  it('should do something', function () {
    expect(!!Pickupstation).toBe(true);
  });

});
