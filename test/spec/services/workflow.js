'use strict';

describe('Service: Workflow', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Workflow;
  beforeEach(inject(function (_Workflow_) {
    Workflow = _Workflow_;
  }));

  it('should do something', function () {
    expect(!!Workflow).toBe(true);
  });

});
