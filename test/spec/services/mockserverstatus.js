'use strict';

describe('Service: Mockserverstatus', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Mockserverstatus;
  beforeEach(inject(function (_Mockserverstatus_) {
    Mockserverstatus = _Mockserverstatus_;
  }));

  it('should do something', function () {
    expect(!!Mockserverstatus).toBe(true);
  });

});
