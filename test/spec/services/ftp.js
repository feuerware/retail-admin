'use strict';

describe('Service: Ftp', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Ftp;
  beforeEach(inject(function (_Ftp_) {
    Ftp = _Ftp_;
  }));

  it('should do something', function () {
    expect(!!Ftp).toBe(true);
  });

});
