'use strict';

describe('Service: Keyvalues', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Keyvalues;
  beforeEach(inject(function (_Keyvalues_) {
    Keyvalues = _Keyvalues_;
  }));

  it('should do something', function () {
    expect(!!Keyvalues).toBe(true);
  });

});
