'use strict';

describe('Service: Corporate', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Corporate;
  beforeEach(inject(function (_Corporate_) {
    Corporate = _Corporate_;
  }));

  it('should do something', function () {
    expect(!!Corporate).toBe(true);
  });

});
