'use strict';

describe('Service: Updatebackend', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Updatebackend;
  beforeEach(inject(function (_Updatebackend_) {
    Updatebackend = _Updatebackend_;
  }));

  it('should do something', function () {
    expect(!!Updatebackend).toBe(true);
  });

});
