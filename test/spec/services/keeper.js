'use strict';

describe('Service: keeper', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var keeper;
  beforeEach(inject(function (_keeper_) {
    keeper = _keeper_;
  }));

  it('should do something', function () {
    expect(!!keeper).toBe(true);
  });

});
