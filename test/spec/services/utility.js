'use strict';

describe('Service: Utility', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var utility;
  beforeEach(inject(function (_utility_) {
    utility = _utility_;
  }));

  it('should do something', function () {
    expect(!!utility).toBe(true);
  });

});
