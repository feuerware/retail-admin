'use strict';

describe('Service: Mockuser', function () {

  // load the service's module
  beforeEach(module('cubeAdminApp'));

  // instantiate service
  var Mockuser;
  beforeEach(inject(function (_Mockuser_) {
    Mockuser = _Mockuser_;
  }));

  it('should do something', function () {
    expect(!!Mockuser).toBe(true);
  });

});
