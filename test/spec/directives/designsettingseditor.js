'use strict';

describe('Directive: DesignSettingsEditor', function () {

  // load the directive's module
  beforeEach(module('cubeAdminApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<-design-settings-editor></-design-settings-editor>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the DesignSettingsEditor directive');
  }));
});
