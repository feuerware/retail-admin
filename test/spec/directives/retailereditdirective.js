'use strict';

describe('Directive: RetailerEditDirective', function () {

  // load the directive's module
  beforeEach(module('cubeAdminApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<-retailer-edit-directive></-retailer-edit-directive>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the RetailerEditDirective directive');
  }));
});
