'use strict';

angular.module('cubeAdminApp')
    .filter('ClientShort', function () {
        return function (input) {
            return input && input.id && input.name && '<a href="#/client/' + input.id + '">' + input.name +
                '</a>' || '';
        };
    });
