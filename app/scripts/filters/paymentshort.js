'use strict';

angular.module('cubeAdminApp')
    .filter('PaymentShort', function () {
        return function (input) {
            return input.paymentType;
        };
    });
