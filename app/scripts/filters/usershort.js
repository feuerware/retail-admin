'use strict';

angular.module('cubeAdminApp')
    .filter('UserShort', function () {
        return function (input) {

            return input && ('<a href="#/user/' + input.id + '">' + input.firstName + ' ' + input.lastName + '</a>') || '';
        };
    });
