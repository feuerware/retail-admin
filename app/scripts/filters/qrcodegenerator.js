'use strict';

angular.module('cubeAdminApp')
    .filter('QrCodeGenerator', function (Helper) {
        return function (input) {
            return '<img width=96 height=96 src="' + Helper.getApi() + '/QrCode/Generator/QR_CODE.png?value=' + input + '">';
        };
    });
