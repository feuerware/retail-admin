'use strict';

angular.module('cubeAdminApp')
    .filter('unsafe', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };

    });
