'use strict';

angular.module('cubeAdminApp')
    .filter('DesignSettingsShort', function ($sce) {

        var hexToColor = function (inputColor) {
            return inputColor.replace('0x', '#');
        };
        var getColorElement = function (inputColor, textColor, name) {
            return '<div style=\'color:' + hexToColor(textColor) + ';background-color:' + hexToColor(inputColor) + '\'>' + name + ' ' + inputColor + '</div>';
        };


        return function (input) {

            return $sce.trustAsHtml(getColorElement(input.backgroundColor, input.textColor, 'BackgroundColor') +
                getColorElement(input.keyColor, input.textColor, 'KeyColor') +
                getColorElement(input.listBackgroundColor1, input.textColor, 'ListColor 1') +
                getColorElement(input.listBackgroundColor2, input.textColor, 'ListColor 2') +
                getColorElement(input.textColor, input.listBackgroundColor1, 'TextColor'));
        };
    });
