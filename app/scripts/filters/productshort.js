'use strict';

angular.module('cubeAdminApp')
    .filter('ProductShort', function () {
        return function (input) {
            return input && '<a href="#/product/' + input.id + '">' + input.name + '</a>';
        };
    });
