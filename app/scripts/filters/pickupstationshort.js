'use strict';

angular.module('cubeAdminApp')
    .filter('PickupstationShort', function () {
        return function (input) {
            return input && '<a href="#/pickupstation/' + input.id + '">' + input.name + '</a>';
        };
    });
