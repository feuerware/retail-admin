'use strict';

angular.module('cubeAdminApp')
    .filter('BuildingAddressShort', function ($sce) {
        return function (input) {
            return input && input.address && $sce.trustAsHtml('<a href=\'http://maps.google.com/?q=' + input.address.location.lat + ',' + input.address.location.lon + '\' target=\'new\'>' + input.address.street + ' ' + input.address.houseNumber + ', ' + input.address.postalCode + ' ' + input.address.city + '</a>') || '';
        };
    });
