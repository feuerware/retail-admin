'use strict';

/**
 * @ngdoc function
 * @name cubeAdminApp.controller:CorporateCtrl
 * @description
 * # CorporateCtrl
 * Controller of the cubeAdminApp
 */
angular.module('cubeAdminApp')
    .controller('CorporateCtrl', ['$scope', 'Corporate', 'SimpleGenerator', 'CorporateMethods', function CorporateCtrl($scope, Corporate, SimpleGenerator, CorporateMethods) {

        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Corporate',
            tableName: 'corporate',
            showCreateDefaultButton: true,
            columns: [
                { prop: 'name', title: 'Name' }

            ],
            sort: 'metaData.lastChangedTimestamp',
            direction: 'DESC',
            restService: Corporate
        });

        $scope.importDefaultTemplates = CorporateMethods.importDefaultTemplates;


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);

    }]).
    controller('CorporateDetailsCtrl', ['$scope', 'Corporate', '$routeParams', 'SimpleGenerator', 'CorporateMethods', 'Dialogs', '$route', 'Retailer', '$q', 'Pickupstation', 'Page', 'StartPage', 'Utility', function ($scope, Corporate, $routeParams, SimpleGenerator, CorporateMethods, Dialogs, $route, Retailer, $q, Pickupstation, Page, StartPage, Utility) {

        console.log('Details Controller erzeugt');

        // Initialise Local List of Objectified RetailerIds
        $scope.currentTemplates = [];
        $scope.currentStartPages = [];
        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Corporate',
            viewTitleField: 'name',
            id: $routeParams.id,
            restService: Corporate,
            restServiceDetailsFuncGet: function (params) {

                // Cleaning out unneded CorporateDesigneSettings variables the result is cleaned up in this modified get method

                // For requesting a single Object, rename "id" to "singleId" due to conflicts with tableview
                //     params["singleId"] = params["id"];

                return Corporate.get(params, function () {



                    // Get list of templates for corporate
                    $scope.$watch('data.id', function (value) {
                        if (angular.isDefined(value)) {

                            Page.getTemplatesForCorporate(value).then(function (data) {
                                $scope.currentTemplates = data;
                            });

                        }
                    });
                    // Get list of startpages for corporate
                    $scope.$watch('data.id', function (value) {
                        if (angular.isDefined(value)) {

                            StartPage.getStartPagesForCorporate(value).then(function (data) {
                                $scope.currentStartPages = data;
                            });

                        }
                    });

                });
            }});
        // Throughput of search functionality for typeaheads
        $scope.findRetailers = Retailer.findRetailers;
        $scope.findPickupStations = Pickupstation.findPickupStations;
        $scope.importDefaultCorporateTemplates = CorporateMethods.importDefaultCorporateTemplates;
        $scope.appendItemToArray = Utility.appendItemToArray;
        $scope.removeItemFromArray = Utility.removeItemFromArray;


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);


    }

    ])
;

