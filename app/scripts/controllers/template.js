'use strict';


angular.module('cubeAdminApp')
    .controller('PageCtrl', ['$scope', 'Page', 'SimpleGenerator', 'Globals', 'Corporate', function ($scope, Page, SimpleGenerator, Globals, Corporate) {


        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Webseiten&Templates',
            tableName: 'template',
            showCreateDefaultButton: true,
            idField: 'id',
            idTitle: 'Template-ID',
            idLink: 'id',
            columns: [
                { prop: 'corporateId', title: 'CorporateId'},
                { prop: 'name', propFilterHidden: false, title: 'Name', type: 'htmlfiltered'},
                { prop: 'corporate', propFilterHidden: true, title: 'Corporate', type: 'htmlfiltered', transform: function (value) {
                    return value && value.name || 'undefined';
                } },
                { prop: 'type', title: 'Typ' },
                { prop: 'title', title: 'Titel', propFilterHidden: true }
            ],
            restService: Page,
            restServiceFuncGet: function (params) {


                return Page.get(params, function (value) {

                    // Get Associated Corporate Object
                    var pageSetCorporate = function (Corporate, page) {
                        var corporateId = page.corporateId;
                        Corporate.get({singleId: corporateId}, function (resultcorporate) {
                            page.corporate = resultcorporate && resultcorporate.result && resultcorporate.result[0] || null;
                        });
                    };

                    for (var index in value.result[0].list) {

                        var page = value.result[0].list[index];
                        pageSetCorporate(Corporate, page);


                    }


                });
            }
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);
    }]).
    controller('PageDetailsCtrl', ['$scope', 'Page', '$routeParams', 'SimpleGenerator', 'Enums', 'Ftp', '$q', 'Globals', 'Dialogs', '$templateCache', function ($scope, Page, $routeParams, SimpleGenerator, Enums, Ftp, $q, Globals, Dialogs, $templateCache) {

        Globals.globalspromise.then(function (resultglobals) {


            $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
                viewTitle: 'Webseite',
                viewTitleField: 'id',
                id: $routeParams.id,
                restService: Page,
                onSavedSuccessfully: function () {
                }, restServiceDetailsFuncGet: function (params) {
                    return Page.get(params, function (value) {


                        var p = value.result[0];
                        return p;
                    });
                }
            });


            $scope.visibilities = Enums.getPageVisibilityStates();
            $scope.types = Enums.getPageTypes();


            $scope.findImages = function (value) {
                var deffered = $q.defer();


                Ftp.get({name: value}, function (result) {
                    var cats = result.result[0].list;
                    angular.forEach(cats, function (cat) {
                        cat.imageUrlStatic = resultglobals.result[0].imageBaseUrl + cat.name;

                    });
                    deffered.resolve(cats);
                    return cats;
                });

                return deffered.promise;

            };


            $scope.setContentPreviewBySnippet = function (snippet, params) {
                // For requesting a single Object, rename "id" to "singleId" due to conflicts with tableview
                if (typeof params.id !== 'undefined') {
                    params.singleId = params.id;
                }

                var snippetNew = $templateCache.get(snippet);

                angular.forEach(params, function (value, key) {
                    console.log(value);
                    snippetNew = snippetNew.replace('{{' + key + '}}', value);
                });

                $scope.data.contentPreview = snippetNew;//imageUrl + label;
            };


            SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);


        });


    }]);