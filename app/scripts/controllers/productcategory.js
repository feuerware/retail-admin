'use strict';

angular.module('cubeAdminApp')
    .controller('ProductCategoryCtrl', ['$scope', 'ProductCategory', 'SimpleGenerator', function ($scope, ProductCategory, SimpleGenerator) {

        //Table View Params used by List View
        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
                tableTitle: 'Produkt-Kategorien',
                tableName: 'productcategory',
                showCreateDefaultButton: true,
                columns: [
                    { prop: 'name', title: 'Name' },
                    { prop: 'totalChildCount', title: 'Kindkategorien', propFilterHidden: true },
                    { prop: 'totalProductCount', title: 'Produkte', propFilterHidden: true},
                    { prop: 'pictures', title: 'Bild', type: 'picarray' }
                ],
                restService: ProductCategory,
                restServiceFuncGet: ProductCategory.get

            }
        );


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);
    }]).
    controller('ProductCategoryDetailsCtrl', ['$scope', 'ProductCategory', '$routeParams', 'SimpleGenerator', 'Retailer', 'Product', function ($scope, ProductCategory, $routeParams, SimpleGenerator, Retailer, Product) {

        // the full blown retailer object resides here
        $scope.currentRetailer = undefined;
        // full blown list of products in category
        $scope.productsInCategory = [];
        // full blown offers in category
        $scope.offersInCategory = [];
        // full blown recommendations in category
        $scope.recommendationsInCategory = [];

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Kategorie',
            viewTitleField: 'name',
            id: $routeParams.id,
            restService: ProductCategory,
            restServiceDetailsFuncGet: function (params) {

                // For requesting a single Object, rename "id" to "singleId" due to conflicts with tableview
                // params["singleId"] = params["id"];

                return ProductCategory.get(params, function (value) {
                    var p = value.result[0];
                    $scope.offersInCategory = p.offers;
                    delete p.offers;
                    $scope.recommendationsInCategory = p.recommendations;
                    delete p.recommendations;


                    Product.get({productCategories: p.id}, function (result) {
                        $scope.productsInCategory = result.result[0].list;

                    });

                    $scope.$watch('currentRetailer', function (value) {
                        if (angular.isDefined(value) && angular.isDefined(value.id)) {
                            $scope.data.retailerID = value.id;
                        }
                    });
                    Retailer.get({id: p.retailerID}, function (value) {
                        $scope.currentRetailer = value.result[0].list[0];
                    });


                    ProductCategory.get({parentCategoryId: p.id, limit: 1000 }, function (value) {
                        $scope.childCategories = value.result[0].list;
                    });


                    return p;

                });
            }
        });
        // Throughput of search functionality for typeaheads
        $scope.findRetailer = Retailer.findRetailers;
        $scope.findCategory = function (value) {
            // wrap into service function filtered by retailer
            return ProductCategory.findProductCategoryForRetailer(value, $scope.data.retailerID);
        };

        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);