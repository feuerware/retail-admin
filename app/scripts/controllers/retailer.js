'use strict';

angular.module('cubeAdminApp')
    .controller('RetailerCtrl', ['$scope', 'Retailer', 'SimpleGenerator', function ($scope, Retailer, SimpleGenerator) {
        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Retailer',
            tableName: 'retailer',
            showCreateDefaultButton: true,
            columns: [
                { prop: 'name', title: 'Name' },
                { prop: 'client', title: 'Mandant', type: 'htmlfiltered', filter: 'ClientShort', propFilter: 'client.name', propFilterHidden: true }
            ],
            restService: Retailer
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);

    }]).
    controller('RetailerDetailsCtrl', ['$scope', 'Retailer', '$routeParams', 'SimpleGenerator', 'Helper', 'Utility', 'ProductCategory', 'Pickupstation', 'Corporate', function ($scope, Retailer, $routeParams, SimpleGenerator, Helper, Utility, ProductCategory, Pickupstation, Corporate) {

        $scope.currentRetailer = {};
        $scope.currentPickupStations = [];
        $scope.currentRootCategory = [];
        $scope.associatedCorporates = [];
        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Retailer',
            viewTitleField: 'name',
            id: $routeParams.id,
            restService: Retailer,
            restServiceDetailsFuncGet: function (params) {
                // For requesting a single Object, rename "id" to "singleId" due to conflicts with tableview
                // params["singleId"] = params["id"];

                return Retailer.get(params, function (value) {

                    $scope.currentRetailer = value.result[0];
                    // Obtain the full blown objects for all refereed retailerids)
                    if (angular.isDefined($scope.currentRetailer.validPickupStationIds)) {

                        var watchFunctionforPickupstationByIndex = function (index, pickupstationid) {
                            $scope.$watch('currentPickupStations[' + index + ']', function (value) {
                                if (angular.isDefined(value) && angular.isDefined(value.id)) {
                                    $scope.data.validPickupStationIds[index] = value.id;
                                }
                            });
                            Pickupstation.get({id: pickupstationid}, function (value) {

                                $scope.currentPickupStations[index] = value.result[0].list[0];
                            });
                        };

                        for (var i = 0; i < $scope.currentRetailer.validPickupStationIds.length; i++) {
                            var pickupstationid = $scope.currentRetailer.validPickupStationIds[i];

                            watchFunctionforPickupstationByIndex(i, pickupstationid);

                        }

                    }
                    $scope.$watch('currentRootCategory', function (value) {
                        if (angular.isDefined(value) && angular.isDefined(value.id)) {
                            $scope.currentRetailer.rootCategoryId = value.id;
                        }
                    });
                    ProductCategory.get({id: $scope.currentRetailer.rootCategoryId }, function (value) {

                        $scope.currentRootCategory = value.result[0].list[0];
                    });
                    // AND get a nice list of all associated corporates
                    // var ret = value.result[0];
                    // deprecated    Helper.cleanBackendDesignSettings(ret.designSettings);

                    Corporate.get({'retailerIDs': $scope.currentRetailer.id }, function (value) {

                        $scope.associatedCorporates = value.result[0].list;
                    });

                });
            }
        });


// Throughput of search functionality for typeaheads
        $scope.findPickupStations = Pickupstation.findPickupStations;
        $scope.findCategories = ProductCategory.findProductCategory;
        $scope.appendItemToArray = Utility.appendItemToArray;
        $scope.removeItemFromArray = Utility.removeItemFromArray;
        $scope.hexToColor = Helper.hexToColor;

        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }])
;