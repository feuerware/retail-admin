'use strict';

angular.module('cubeAdminApp')
    .controller('ClientCtrl', ['$scope', 'Client', 'SimpleGenerator', function ($scope, Client, SimpleGenerator) {
        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Mandanten',
            tableName: 'client',
            showCreateDefaultButton: true,
            columns: [
                { prop: 'name', title: 'Name' },

                { prop: 'checkMinimumBasketPrice', title: 'Mindest-Aktiv?', type: 'boolean' },
                /*          { prop: 'deliveryCostCents', title: 'Lieferkosten', type: 'htmlfiltered', transform: function (input) {
                 return input / 100;
                 } },
                 { prop: 'designSettings', title: 'Design-Settings', type: 'htmlfiltered', filter: 'DesignSettingsShort'}       */
            ],
            restService: Client
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);

    }]).
    controller('ClientDetailsCtrl', ['$scope', 'Client', '$routeParams', 'SimpleGenerator', 'Helper', function ($scope, Client, $routeParams, SimpleGenerator, Helper) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Mandanten',
            viewTitleField: 'name',
            id: $routeParams.id,
            restService: Client,
            restServiceDetailsFuncGet: function (params) {
                return Client.get(params, function (value) {
                    var client = value.result[0];
                    Helper.cleanBackendDesignSettings(client.designSettings);
                });
            }
        });


        $scope.setDefaultDeliveryCosts = function () {
            $scope.data.deliveryCostInfo = $scope.data.defaultDeliveryCostInfo;
        };
        $scope.hexToColor = Helper.hexToColor;


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);


