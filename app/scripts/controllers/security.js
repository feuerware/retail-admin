'use strict';

angular.module('cubeAdminApp').
    controller('UserCurrentCtrl', ['$modal', '$location', '$scope', '$route', '$rootScope', 'Security', 'Helper', 'Dialogs', function ($modal, $location, $scope, $route, $rootScope, Security, Helper, Dialogs) {

        var showDialog = function () {


            var ModalInstanceCtrl = function ($scope, $location, $templateCache, $rootScope, $route, Security, Helper, Dialogs) {
                console.log('UsrLoginCtrl');
                $scope.showError = false;


                $scope.user = { email: Security.getSavedEmail(), password: ''};


                $scope.apis = Helper.getApis();
                var currentHost = Helper.loadHostFromStorageOrUseDefault();
                for (var i = 0; i < $scope.apis.length; i++) {
                    var a = $scope.apis[i];
                    if (a.address === currentHost.address) {
                        $scope.api = $scope.apis[i];
                        break;
                    }
                }


                $scope.login = function (api, email, password) {
                    Helper.selectHost(api);

                    console.log(api);
                    Security.saveEmail(email);

                    if (email && password) {


                        var hash = CryptoJS.SHA256(password);
                        var md5Password = hash.toString(CryptoJS.enc.Hex);

                        Security.savePassword(md5Password);

                        Security.tryLogin(email, md5Password).$promise.then(
                            //success
                            function (value) {
                                if (Helper.isResponseOK(value)) {

                                    if (value.result[0].role === 'ROLE_ADMIN') {
                                        $rootScope.isLoggedIn = true;
                                        //$templateCache.removeAll();

                                        $rootScope.currentUser = value.result[0];
                                        Security.setToken($rootScope.currentUser.id);

                                        Security.fullPageReload();
                                    }
                                    else {
                                        Dialogs.showOKDialog('Fehler', 'Du bist kein Admin !!!');
                                    }


                                } else {
                                    if (value.info.statusCode === 5500) {
                                        Dialogs.showOKDialog('Fehler', 'Email oder Passwort falsch');

                                    } else {
                                        Dialogs.showOKDialog('Fehler', 'Ein unbekannter Fehler ist aufgetreten');
                                    }
                                }


                            },
                            //error
                            function (error) {


                                Dialogs.showErrorDialogForResponse(error);


                            }
                        );
                    } else {
                        Dialogs.showOKDialog('Fehler', 'Email oder Passwort nicht angegeben');
                    }


                };


            };

            console.log('a');
            $modal.open({
                templateUrl: 'views/loginmodal.html',
                controller: ModalInstanceCtrl
            });
        };


        $rootScope.logout = function () {


            Dialogs.showYESNODialog('Logout?', 'Sind sie sicher?', function (okay) {

                if (okay) {

                    Security.logout();
                    $rootScope.isLoggedIn = false;
                    Security.fullPageReload();

                }


            });

        };
        $rootScope.currentHost = Helper.getHost();

        var loggingIn = Security.getLocalUser();

        if (loggingIn !== null) {
            loggingIn.$promise.then(
                //success
                function (value) {
                    if (Helper.isResponseOK(value)) {
                        $rootScope.currentUser = value.result[0];
                        Security.setToken($rootScope.currentUser.id);
                    } else {

                    }
                },
                //error
                function (error) {
                    Dialogs.showErrorDialogForResponse(error);

                    showDialog();
                }
            );
        }
        else {
            showDialog();
        }


    }
    ])
;