'use strict';

/**
 * @ngdoc function
 * @name cubeAdminApp.controller:StatisticsCtrl
 * @description
 * # StatisticsCtrl
 * Controller of the cubeAdminApp
 */
angular.module('cubeAdminApp')
    .controller('StatisticsCtrl', ['$scope', 'Order', 'User', '$filter', 'Product', function ($scope, Order, User, $filter, Product) {

        $scope.reportFileName = 'orders';
        $scope.debugText = '';
        $scope.currentTab = 'orders';

        var isOutOfRange = function (orderDate) {
            if ($scope.useDateFilter && angular.isDefined($scope.dtStart) && angular.isDefined($scope.dtEnd)) {

                if (orderDate < $scope.dtStart || orderDate > $scope.dtEnd) {
                    return true;
                }
            }

            return false;
        };

        var writeOut = function (value) {
            var firstLine = 'OrderId;OrderDate;OrderValueOfGoodsCentBrutto;ProductExternalId;ProductName;ProductPriceCentBrutto;ProductCount\n';

            var csvOrdersTemp = firstLine;

            var getOrderFunction = function (order) {


                var line = '';
                line += order.id + ';';
                line += $filter('date')(order.creationTime, 'dd.MM.yyyy HH:mm:ss') + ';';

                var valueOfGoods = 'unbekannt';
                for (var priceIndex = 0; priceIndex < order.prices.length; priceIndex++) {
                    var price = order.prices[priceIndex];
                    if (price.priceType === 'VALUEOFGOODS') {
                        valueOfGoods = price.centPriceBrutto;
                    }
                }

                line += valueOfGoods + ';';
                var baseLine = line;


                for (var itemIndex = 0; itemIndex < order.items.length; itemIndex++) {
                    line = '';

                    var item = order.items[itemIndex];
                    var product = item.product;


                    if (typeof $scope.products[product.id] === 'undefined') {
                        line += 'unknown;';
                        $scope.debugText += 'Fehler: Bei "' + product.name + '" konnte die externe Product-ID nicht ermittelt werden. Id wurde auf "unknown" gesetzt\n';
                    } else {
                        line += $scope.products[product.id].externalProductIdentifier + ';';
                    }
                    line += product.name + ';';
                    for (var productPriceIndex = 0; productPriceIndex < product.price.length; productPriceIndex++) {
                        var productPrice = product.price[productPriceIndex];
                        if (productPrice.priceType === 'PRICE') {
                            line += productPrice.centPriceBrutto + ';';
                        }
                    }
                    line += item.count + ';';

                    csvOrdersTemp += baseLine + line.substring(0, line.length - 1) + '\n';

                }

                //console.log(order);

            };


            for (var index = 0; index < value.result[0].list.length; index++) {

                var order = value.result[0].list[index];


                if (isOutOfRange(new Date(order.creationTime))) {
                    continue;
                }

                getOrderFunction(order);


            }

            $scope.debugText += 'Berechnung abgeschlossen';

            $scope.csvOrders = csvOrdersTemp;
            $scope.csvOrdersLoadedOnce = true;
            $scope.data = value.result[0];
            $scope.loading = false;
            $scope.reportFileName = 'orders';

        };

        $scope.products = {};
        $scope.calculateOrderStats = function () {
            $scope.loading = true;
            var params = {};
            params.direction = 'DESC';
            params.limit = 999999;
            params.page = 0;
            params.sort = 'creationTime';
            $scope.currentTab = 'orders';
            $scope.debugText = '';

            Order.get(params).$promise.then(
                //success
                function (value) {


                    $scope.csvOrders = '';

                    $scope.toCount = 0;

                    var getOrderAdditionalDataFunction = function (order) {

                        var getProductFunction = function (product) {
                            Product.get({id: product.id}, function (v) {
                                if (v.result && v.result[0]) {
                                    $scope.products[product.id] = v.result[0];
                                }


                                $scope.toCount--;
                                console.log('-');
                                if ($scope.toCount === 0) {
                                    console.log(value);
                                    writeOut(value);
                                }
                            }, function () {
                                $scope.toCount--;
                                console.log('-');
                                if ($scope.toCount === 0) {
                                    console.log(value);
                                    writeOut(value);
                                }
                            });
                        };

                        for (var itemIndex = 0; itemIndex < order.items.length; itemIndex++) {
                            $scope.toCount++;
                            console.log('+');

                            var item = order.items[itemIndex];
                            var product = item.product;

                            if (typeof $scope.products[product.id] === 'undefined') {

                                getProductFunction(product);
                            } else {
                                $scope.toCount--;
                                console.log('-');
                                if ($scope.toCount === 0) {
                                    console.log(value);
                                    writeOut(value);
                                }
                            }

                        }
                    };


                    for (var index = 0; index < value.result[0].list.length; index++) {


                        var order = value.result[0].list[index];


                        getOrderAdditionalDataFunction(order);


                    }


                },
                //error
                function () {
                    $scope.loading = false;
                }
            );
        };


        $scope.configOrderBasketPrice = {
            'labels': false,
            'title': 'Verteilung Bestellmenge in € (Brutto)',
            'legend': {
                'display': true,
                'position': 'right'
            },
            'innerRadius': 0,
            'lineLegend': 'traditional'
        };


        $scope.configOrderBasketPriceViewData = {
            'series': [
                'Verteilung Bestellmengen in € (Brutto)'
            ],
            data: [
                {'x': ' <= 4.99 €', 'y': [1]},
                {'x': '5 - 9.99 €', 'y': [1] },
                {'x': '10 - 39.99 €', 'y': [1] },
                {'x': '40 - 99.99 €', 'y': [1] },
                {'x': '>= 100 €', 'y': [1]}
            ],
            temp: [0, 0, 0, 0, 0],
            reset: function () {
                for (var index = 0; index < this.data.length; index++) {
                    this.temp[index] = 0;
                }
            },
            addEntryByCents: function (cents) {

                var index = 0;
                if (cents < 500) {
                    index = 0;
                } else if (cents < 1000) {
                    index = 1;
                } else if (cents < 4000) {
                    index = 2;
                } else if (cents < 10000) {
                    index = 3;
                } else {
                    index = 4;
                }

                this.temp[index]++;
            },
            updateUI: function () {
                for (var index = 0; index < this.temp.length; index++) {
                    this.data[index].y[0] = this.temp[index];
                }
            }


        };


        $scope.configOrderOverview = {
            'labels': false,
            'title': 'Bestell-Verlauf',
            'legend': {
                'display': true,
                'position': 'right'
            },
            'innerRadius': 0,
            'lineLegend': 'traditional'
        };


        $scope.configOrderOverViewData = {
            'series': [
                'Anzahl Produkte',
                'Bestellwert € (Brutto)'
            ],
            'data': [

            ]
        };


        $scope.calculateOrderOverviewStats = function () {
            $scope.loadingOverview = true;
            var params = {};
            params.direction = 'DESC';
            params.limit = 999999;
            params.page = 0;
            params.sort = 'creationTime';
            $scope.debugText = '';
            $scope.averageOrderValue = 0;
            $scope.configOrderBasketPriceViewData.reset();
            $scope.currentTab = 'overview';
            $scope.configOrderOverViewData.data = [];
            Order.get(params).$promise.then(
                //success
                function (value) {

                    var firstLine = 'OrderDate;OrderValueOfGoodsCentBrutto;OrderProductCount\n';
                    var csvOrdersTemp = firstLine;
                    var avgOrderValueTemp = 0;
                    var countAvgOrderValue = 0;
                    $scope.csvOrders = '';

                    $scope.toCountOverview = value.result[0].list.length;
                    for (var index = 0; index < value.result[0].list.length; index++) {

                        var order = value.result[0].list[index];
                        $scope.toCountOverview--;


                        if (isOutOfRange(new Date(order.creationTime))) {
                            continue;
                        }

                        var line = '';
                        line += $filter('date')(order.creationTime, 'dd.MM.yyyy HH:mm:ss') + ';';

                        var valueOfGoods = 'unbekannt';
                        for (var priceIndex = 0; priceIndex < order.prices.length; priceIndex++) {
                            var price = order.prices[priceIndex];
                            if (price.priceType === 'VALUEOFGOODS') {
                                valueOfGoods = price.centPriceBrutto;
                            }
                        }

                        line += valueOfGoods + ';';
                        var productsCount = 0;

                        for (var itemIndex = 0; itemIndex < order.items.length; itemIndex++) {
                            var item = order.items[itemIndex];

                            productsCount += item.count;
                        }

                        line += productsCount + ';';

                        csvOrdersTemp += line.substring(0, line.length - 1) + '\n';


                        $scope.configOrderOverViewData.data.push({'x': index + '', 'y': [productsCount, valueOfGoods / 100]});
                        if ($scope.configOrderOverViewData.data.length > 20) {
                            $scope.configOrderOverViewData.data.shift();
                        }

                        $scope.configOrderBasketPriceViewData.addEntryByCents(valueOfGoods);


                        avgOrderValueTemp += valueOfGoods;
                        countAvgOrderValue++;

                    }

                    $scope.averageOrderValue = parseFloat(((avgOrderValueTemp / countAvgOrderValue) / 100) + '').toFixed(2) + ' €';

                    $scope.loadingOverview = false;
                    $scope.debugText += 'Berechnung abgeschlossen';
                    $scope.csvOrders = csvOrdersTemp;
                    $scope.csvOrdersLoadedOnce = true;
                    $scope.reportFileName = 'orders_overview';
                    $scope.configOrderBasketPriceViewData.updateUI();

                },
                //error
                function () {
                    $scope.loadingOverview = false;
                }
            );
        };


        $scope.downloadAsFile = function () {
            var csvString = $scope.csvOrders;
            var a = document.createElement('a');
            a.href = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csvString);
            a.target = '_blank';
            a.download = $scope.reportFileName + '.csv';

            var csvWin = window.open('', '', '');
            csvWin.document.write('<meta name="content-type" content="text/csv; charset=utf-8">');
            csvWin.document.write('<meta name="content-disposition" content="attachment;  filename=' + $scope.reportFileName + '.csv">  ');
            csvWin.document.write(csvString);
            document.body.appendChild(a);
            a.click();
        };


    }]);