'use strict';

angular.module('cubeAdminApp')
    .controller('AppDeviceCtrl', ['$scope', 'AppDevice', 'SimpleGenerator', 'User', function ($scope, AppDevice, SimpleGenerator, User) {
        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'AppDevice',
            tableName: 'appdevice',
            columns: [
                { prop: 'deviceType', title: 'Device-Typ' },
                { prop: 'appId', title: 'App-Id' },
                { prop: 'appVersion', title: 'App-Version' },
                { prop: 'user', title: 'Benutzer', type: 'htmlfiltered', filter: 'UserShort', propFilter: 'user.userName', propFilterHidden: true},
                { prop: 'language', title: 'Sprache' },
                { prop: 'metaData', title: 'Datum', type: 'htmlfiltered', filter: 'date:\'dd.MM.yyyy HH:mm:ss\'', propFilter: 'creationTime', propFilterHidden: true, transform: function (value) {
                    return value.lastChangedTimestamp;
                }},
                { prop: 'token', title: 'Token' }
            ],
            sort: 'metaData.lastChangedTimestamp',
            direction: 'DESC',
            restService: AppDevice,
            restServiceFuncGet: function (params) {


                var userGetResultHandler = function (device) {
                    return function (result) {
                        device.user = result.result[0];

                    };
                };


                return AppDevice.get(params, function (value) {


                    for (var index in value.result[0].list) {

                        var device = value.result[0].list[index];
                        User.get({id: device.userId}, userGetResultHandler(device));


                    }


                });
            }
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);

    }]).
    controller('AppDeviceDetailsCtrl', ['$scope', 'AppDevice', 'AppDevicePushSender', '$routeParams', 'SimpleGenerator', 'Helper', 'User', 'Dialogs', function ($scope, AppDevice, AppDevicePushSender, $routeParams, SimpleGenerator, Helper, User, Dialogs) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'AppDevice',
            viewTitleField: 'id',
            id: $routeParams.id,
            restService: AppDevice,
            restServiceDetailsFuncGet: function (params) {


                return AppDevice.get(params, function (value) {
                    var device = value.result[0];
                    (function (device) {
                        User.get({id: device.userId}, function (result) {
                            $scope.user = result.result[0];

                        });
                    })(device);
                });
            }
        });


        $scope.sendPush = function () {
            AppDevicePushSender.post({id: $scope.data.id}, {}, function (value) {
                if (value.info.statusCode === 100) {
                    Dialogs.showOKDialog('Push Mitteliung erfolgreich abgesetzt', 'Die Nachricht wurde erfolgreich verschickt');
                }
                else {
                    Dialogs.showOKDialog('Fehler', 'Fehler ' + value.info.status + ' (' + value.info.statusCode + ') - Etwas ist schief gegangen.');
                }
            });
        };
        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);