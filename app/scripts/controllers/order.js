'use strict';

angular.module('cubeAdminApp')
    .controller('OrderCtrl', ['$scope', 'Order', 'SimpleGenerator', 'User', function ($scope, Order, SimpleGenerator, User) {


        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Bestellungen',
            tableName: 'order',
            columns: [

                { prop: 'creationTime', title: 'Erstellt am', type: 'htmlfiltered', filter: 'date:\'dd.MM.yyyy HH:mm:ss\'', propFilter: 'creationTime', propFilterHidden: true},
                { prop: 'user', title: 'Kunde', type: 'htmlfiltered', filter: 'UserShort', propFilter: 'user.lastName', propFilterHidden: true},
                { prop: 'state', title: 'Status', propFilter: 'state' },
                { prop: 'pickupStation', title: 'Abholstation', type: 'htmlfiltered', filter: 'PickupstationShort', propFilter: 'pickupStation.name'},
                { prop: 'payment', title: 'Bezahlmodus', type: 'htmlfiltered', propFilter: 'payment.paymentType', filter: 'PaymentShort'},
                { prop: 'pickupCode', title: 'Abholcode', propFilter: 'pickupCode'}
            ],
            restService: Order,
            sort: 'creationTime',
            direction: 'DESC',
            restServiceFuncGet: function (params) {


                return Order.get(params, function (value) {

                    // Retrieves the full user object for each order
                    var getUserForOrderFunction = function (User, order) {
                        var orderUserId = order.userId;
                        User.get({singleId: orderUserId}, function (resultuser) {
                            order.user = resultuser && resultuser.result && resultuser.result[0] || null;
                        });
                    };
                    for (var index in value.result[0].list) {

                        var order = value.result[0].list[index];
                        getUserForOrderFunction(User, order);


                    }


                });
            }
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);
    }]).
    controller('OrderDetailsCtrl', ['$scope', 'Order', '$routeParams', 'SimpleGenerator', 'Enums', 'Pickupstation', 'User', 'Dialogs', 'NotificationLive', 'Workflow', function ($scope, Order, $routeParams, SimpleGenerator, Enums, Pickupstation, User, Dialogs, NotificationLive, Workflow) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Bestellung',
            viewTitleField: 'id',
            id: $routeParams.id,
            restService: Order,
            restServiceDetailsFuncGet: function (params) {


                // For requesting a single Object, rename "id" to "singleId" due to conflicts with tableview
                if (typeof params.id !== 'undefined') {
                    params.singleId = params.id;
                }
                return Order.get(params, function (value) {

                    var order = value.result[0];

                    (function (order) {
                        var orderUserId = order.userId;
                        User.get({id: orderUserId}, function (resultuser) {
                            $scope.currentUser = resultuser && resultuser.result && resultuser.result[0] || null;
                        });

                        Workflow.get({'currentState.contextData.orderId': order.id, sort: 'metaData.creationTimestamp', direction: 'DESC'
                        }, function (value) {
                            $scope.workflows = value.result[0].list;
                        });

                    })(order);


                });
            }
        });

        $scope.getPriceType = function (v) {
            return Enums.getTitleByValue(Enums.getPriceTypes(), v);
        };

        $scope.orderstates = Enums.getOrderStates();


        Pickupstation.get(function (value) {
            $scope.pickupstations = value.result[0].list;
        });

        $scope.sendNotification = function (notificationState, notificationPickupCode, externalId) {

            NotificationLive.post({id: 'order', signupid: externalId, orderstatusid: notificationState, pickupCode: notificationPickupCode}, {}, function (value) {

                if (value.info.statusCode === 100) {
                    Dialogs.showOKDialog('Erfolgreich', 'Notification wurde erfolgreich abgesetzt');
                } else {
                    Dialogs.showOKDialog('Fehler', 'Fehler ' + value.info.status + ' (' + value.info.statusCode + ') - Etwas ist schief gegangen. Datensatz konnte nicht gespeichert werden');
                }
            });
        };

        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);
