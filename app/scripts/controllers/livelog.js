'use strict';

angular.module('cubeAdminApp')
    .controller('LiveLogCtrl', [ '$timeout', '$scope', 'LiveLog', function ($timeout, $scope, LiveLog) {
        $scope.a = 'b';

        $scope.index = 0;
        $scope.logs = [];

        var updateLog = function () {
            LiveLog.get({ id: $scope.index}).$promise.then(
                //success
                function (value) {

                    $scope.logs = value.result[0].list.concat($scope.logs).slice(0, 500);
                    $scope.index = $scope.logs[0].index;
                    $timeout(updateLog, 5000);
                });
        };

        updateLog();


    }]);
