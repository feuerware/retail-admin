'use strict';

angular.module('cubeAdminApp')
    .controller('ServerstatusCtrl', ['$scope', 'ServerStatus', 'Properties', 'ImporterStatus', 'Helper', 'Globals', 'UpdateBackend', 'Dialogs', function ($scope, ServerStatus, Properties, ImporterStatus, Helper, Globals, UpdateBackend, Dialogs) {


        $scope.showColumn = 'live';
        $scope.showColumnSelect = ['live', 'config', 'default'];

        $scope.loading = false;
        $scope.dummy = {};
        $scope.dummy.depth = 3;
        $scope.dummy.leafesProductCount = 3;
        $scope.dummy.childCount = 3;

        $scope.isDemoMode = Helper.getIsDemoMode();
        $scope.host = Helper.getHost();
        Globals.globalspromise.then(function (value) {
            $scope.globals = value.result[0];
        });


        $scope.refreshImporterStatus = function () {
            ImporterStatus.get({'singleId': 'full'}, function (value) {
                $scope.importerStatus = value && value.result && value.result[0].result[0] && value.result[0].result[0].list[0];

            });

        };
        $scope.startImporter = function () {
            $scope.importerStatusStartResponse = ImporterStatus.post({'singleId': 'full'});
            $scope.calculateServerStatus();

        };
        $scope.startDummyDataGeneration = function () {
            $scope.importerStatusStartResponse = ImporterStatus.post({'singleId': 'dummy'});
            $scope.calculateServerStatus();

        };

        $scope.startImporterSwitch = function () {
            $scope.importerStatusStartResponse = ImporterStatus.post({'singleId': 'switch'});
            $scope.calculateServerStatus();

        };
        $scope.startImporterExpand = function () {
            $scope.importerStatusStartResponse = ImporterStatus.post({'singleId': 'expand'});
            $scope.calculateServerStatus();

        };

        $scope.calculateServerStatus = function () {

            $scope.loading = true;
            $scope.data = null;
            ServerStatus.get().$promise.then(
                //success
                function (value) {
                    $scope.loading = false;
                    $scope.data = value.result[0];
                },
                //error
                function () {
                    $scope.loading = false;
                }
            );

        };


        $scope.updateBackend = function () {
            UpdateBackend.post({singleId: 'UpdateAAXUserPasswords'}, {}, function (value) {
                if (value.info.statusCode === 100) {
                    Dialogs.showOKDialog('Änderung erfolgreich', 'Änderungen wurden erfolgreich durchgeführt');
                }
                else {
                    Dialogs.showOKDialog('Fehler', 'Fehler ' + value.info.status + ' (' + value.info.statusCode + ') - Etwas ist schief gegangen. Datensatz konnte nicht gespeichert werden');
                }
            }, function () {
                Dialogs.showOKDialog('Fehler', 'Ein Fehler ist aufgetreten');

            });
        };
        $scope.refreshImporterStatus();

        Properties.get(function (value) {
            $scope.serverProperties = value.result[0];
        });


    }]);
