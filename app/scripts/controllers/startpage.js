'use strict';

angular.module('cubeAdminApp')
    .controller('StartPageCtrl', ['$scope', 'StartPage', 'SimpleGenerator', function ($scope, StartPage, SimpleGenerator) {


        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Startseiten',
            tableName: 'startpage',
            showCreateDefaultButton: true,
            columns: [
                { prop: 'name', title: 'Name' },
                { prop: 'corporateId', title: 'Corporate' }

            ],
            restService: StartPage
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);
    }]).
    controller('StartPageDetailsCtrl', ['$q', '$scope', 'StartPage', '$routeParams', 'SimpleGenerator', 'ProductCategory', 'Globals', 'Enums', 'Page', 'Corporate', '$sce', function ($q, $scope, StartPage, $routeParams, SimpleGenerator, ProductCategory, Globals, Enums, Page, Corporate, $sce) {

        // Retrieve list of Corporates
        Corporate.get({}, function (value) {
            $scope.corporates = value.result[0].list;
        });

        $scope.isImageVisible = function (scat) {

            var cat = $scope.getCategoryByStartPageElement(scat);
            if (angular.isDefined(cat) && angular.isDefined(cat.pictures) && cat.pictures && cat.pictures.length > 0) {
                return true;
            } else {
                return false;
            }
        };

        $scope.isNameVisible = function (scat) {
            var isImageVisible = $scope.isImageVisible(scat);
            if (!isImageVisible) {
                return true;
            }
            else {

                if (scat.layout.search('IMAGETEXT') >= 0) {
                    return true;
                } else {
                    return false;
                }

            }
        };

        $scope.getImageSrc = function (cat) {
            return angular.isDefined(cat) && angular.isDefined(cat.pictures) && cat.pictures && cat.pictures.length > 0 && cat.pictures[0] || null;
        };

        $scope.productCategoyLayouts = Enums.getProductCategoryLayoutHint();

        $scope.getTileClass = function (cat) {
            return cat.layout;
        };
        $scope.categories = {};
        $scope.pages = {};
        var imageBasePath = '';

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Startseite',
            viewTitleField: 'id',
            id: $routeParams.id,
            restService: StartPage,
            restServiceDetailsFuncGet: function (params) {

                // TODO: FIXME: MOVE FUNCTIONALITY TO SERVICES; URL GENERATION SHALL BE DONE IN CATEGORYSERVICE!
                return StartPage.get(params, function (value) {

                    Globals.globalspromise.then(function (resultglobals) {
                        imageBasePath = resultglobals.result[0].imageBaseUrl;


                        var loadCategoryFunction = function (imageBasePath, element) {
                            ProductCategory.get({id: element.categoryId}, function (categoryResult) {
                                var cat = categoryResult.result[0];
                                if (angular.isDefined(cat.id)) {
                                    if (angular.isDefined(cat.pictures) && cat.pictures.length > 0) {
                                        cat.pictures[0].imageUrlStatic = imageBasePath + cat.pictures[0].fileName;
                                    }
                                    $scope.categories[cat.id] = cat;

                                }
                            });
                        };

                        //load categories
                        for (var index in value.result[0].categories) {
                            var element = value.result[0].categories[index];

                            loadCategoryFunction(imageBasePath, element);
                        }

                        var loadMarketingPagesFunction = function (categoryResult) {
                            if (angular.isDefined(categoryResult.result)) {
                                var cat = categoryResult.result[0];
                                if (angular.isDefined(cat.id)) {
                                    $scope.pages[cat.id] = cat;
                                }
                            }
                        };
                        //load marketing pages
                        for (index in value.result[0].webPages) {
                            var webpage = value.result[0].webPages[index];

                            loadMarketingPagesFunction(webpage);
                        }


                    });


                });
            }
        })
        ;

        $scope.getIndexByStartPageElement = function (elem) {
            var index = -1;
            for (var i = 0; i < $scope.data.categories.length; i++) {
                if ($scope.data.categories[i] === elem) {
                    index = i;
                    break;
                }
            }

            return index;
        };

        $scope.getHtmlUnsafe = function (value) {
            console.log('a');
            return $sce.trustAsHtml(value);
        };
        $scope.getCategoryByStartPageElement = function (elem) {
            return $scope.categories[elem.categoryId];
        };
        $scope.deleteStartPageElement = function (elem) {
            var index = $scope.getIndexByStartPageElement(elem);
            $scope.data.categories.splice(index, 1);
        };

        $scope.moveStartPageElement = function (elem, forward) {

            var index = $scope.getIndexByStartPageElement(elem);
            $scope.data.categories.move(index, index + (forward ? 1 : -1));

        };
        $scope.addStartPageElement = function () {
            var newcat = angular.copy($scope.data.categories[$scope.data.categories.length - 1]);
            $scope.data.categories.push(newcat);
        };


        $scope.findCategories = function (value) {
            //$scope.searchedCategories = [];
            var deffered = $q.defer();


            ProductCategory.get({name: value}, function (categoryResult) {
                var cats = categoryResult.result[0].list;
                angular.forEach(cats, function (cat) {
                    if (angular.isDefined(cat.id)) {
                        if (angular.isDefined(cat.pictures) && cat.pictures.length > 0) {
                            cat.pictures[0].imageUrlStatic = imageBasePath + cat.pictures[0].fileName;
                        }
                        $scope.categories[cat.id] = cat;
                    }
                });
                deffered.resolve(cats);
                return cats;
            });

            return deffered.promise;

        };

        $scope.findMarketingPages = function (value) {
            //$scope.searchedCategories = [];
            var deffered = $q.defer();


            Page.get({name: value}, function (categoryResult) {
                var cats = categoryResult.result[0].list;

                angular.forEach(cats, function (cat) {
                    if (angular.isDefined(cat.id)) {
                        $scope.pages[cat.id] = cat;
                    }
                });


                deffered.resolve(cats);
                return cats;
            });

            return deffered.promise;
        };

        var loadAllMarketingPages = function () {
            //$scope.searchedCategories = [];


            Page.get({}, function (categoryResult) {
                var cats = categoryResult.result[0].list;
                $scope.pagesAsList = cats;


            });


        };

        loadAllMarketingPages();

        $scope.addMarketingPage = function () {
            var newPage = {pageId: '', sortIndex: 10};
            if (!$scope.data.webPages) {
                $scope.data.webPages = [];
            }

            $scope.data.webPages.push(newPage);
        };

        $scope.deleteMarketingPage = function (elem) {
            var index = $scope.data.webPages.indexOf(elem);
            $scope.data.webPages.splice(index, 1);
        };


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }])
;
