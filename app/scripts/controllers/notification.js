'use strict';

angular.module('cubeAdminApp')
    .controller('NotificationCtrl', ['$scope', 'Notification', 'SimpleGenerator', function ($scope, Notification, SimpleGenerator) {
        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Notification',
            tableName: 'notification',
            columns: [
                { prop: 'metaData', title: 'Letzte Änderung', type: 'htmlfiltered', filter: 'date:\'dd.MM.yyyy HH:mm:ss\'', propFilter: 'lastChangedTimestamp', propFilterHidden: true, transform: function (value) {
                    return value.lastChangedTimestamp;
                }},

                { prop: 'notificationName', title: 'Name' },
                { prop: 'notificationState', title: 'Zustand' }
            ],
            sort: 'metaData.lastChangedTimestamp',
            direction: 'DESC',
            restService: Notification,
            restServiceFuncGet: function (params) {


                return Notification.get(params, function () {


                });
            }
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);

    }]).
    controller('NotificationDetailsCtrl', ['$scope', 'Notification', '$routeParams', 'SimpleGenerator', 'Helper', 'Enums', function ($scope, Notification, $routeParams, SimpleGenerator, Helper, Enums) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Notification',
            viewTitleField: 'id',
            id: $routeParams.id,
            restService: Notification,
            restServiceDetailsFuncGet: function (params) {

                // For requesting a single Object, rename "id" to "singleId" due to conflicts with tableview
                params.singleId = params.id;

                return Notification.get(params, function () {
                });
            }
        });

        $scope.states = Enums.getNotificationStates();


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);
