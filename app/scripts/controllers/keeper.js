'use strict';

/**
 * @ngdoc function
 * @name cubeAdminApp.controller:KeeperCtrl
 * @description
 * # KeeperCtrl
 * Controller of the cubeAdminApp
 */
angular.module('cubeAdminApp')
    .controller('KeeperCtrl', ['$scope', 'Keeper', 'SimpleGenerator', function KeeperCtrl($scope, Keeper, SimpleGenerator) {

        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Import-Sperren',
            tableName: 'keeper',
            showCreateDefaultButton: true,
            columns: [
                { prop: 'targetTypeName', title: 'ZielObjekt-Typ' },
                { prop: 'targetId', title: 'ZielObjekt-Id' },
                { prop: 'deletionLock', title: 'Lösch-Sperre', type: 'boolean' },
                { prop: 'propertyLock', title: 'Eigengschaften-Sperre', type: 'boolean' }
            ],
            sort: 'metaData.lastChangedTimestamp',
            direction: 'DESC',
            restService: Keeper
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);

    }]).
    controller('KeeperDetailsCtrl', ['$scope', 'Keeper', 'SimpleGenerator', 'Helper', '$routeParams', 'Enums', function ($scope, Keeper, SimpleGenerator, Helper, $routeParams, Enums) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Keeper',
            viewTitleField: 'id',
            id: $routeParams.id,
            restService: Keeper
        });
        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

        $scope.targettypes = Enums.getKeeperTargetTypes();
        $scope.getTargetTypePath = function (targetType) {

            for (var i = 0; i < $scope.targettypes.length; i++) {
                var type = $scope.targettypes[i];
                if (type.value === targetType) {
                    return type.path;
                }
            }

            return '';
        };

        $scope.addProperty = function () {
            $scope.data.propertiesToKeep.push('name');
        };

        $scope.remLastProperty = function () {
            $scope.data.propertiesToKeep.pop();
        };

    }]);

