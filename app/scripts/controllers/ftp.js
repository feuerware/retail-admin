'use strict';

angular.module('cubeAdminApp')
    .controller('FtpCtrl', ['$scope', 'Ftp', 'SimpleGenerator', 'Globals', '$fileUploader', 'Security', 'Dialogs', function ($scope, Ftp, SimpleGenerator, Globals, $fileUploader, Security, Dialogs) {

        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'MedienServer',
            tableName: 'ftp',
            columns: [
                { prop: 'size', title: 'Dateigröße', type: 'htmlfiltered', propFilterHidden: true, transform: function (value) {
                    return (value / 1024).toFixed(0) + ' kb';
                }},
                { prop: 'timestamp', title: 'Erstellt am', type: 'htmlfiltered', filter: 'date:\'dd.MM.yyyy HH:mm:ss\'', propFilter: 'timestamp', propFilterHidden: true},
                { prop: 'imageUrlStatic', title: 'Vorschau', type: 'img' }

            ],
            idField: 'name',
            idLink: 'safeName',
            idTitle: 'Filename',
            restService: Ftp,
            restServiceFuncGet: function (params) {
                var ftpFileFunction = function (resultglobals, ftpFile) {
                    ftpFile.imageUrlStatic = resultglobals.result[0].imageBaseUrl + ftpFile.name;
                    //.jpg makes problems for angularjs 406 error (json for image)
                    ftpFile.safeName = ftpFile.name + '_.json';
                };

                return Ftp.get(params, function (value) {
                    Globals.globalspromise.then(function (resultglobals) {
                        for (var index in value.result[0].list) {
                            var ftpFile = value.result[0].list[index];
                            ftpFileFunction(resultglobals, ftpFile);
                        }
                    });

                });
            },
            sort: 'timestamp',
            direction: 'DESC'
        });

        var uploader = $scope.uploader = $fileUploader.create({
            scope: $scope,
            url: null,
            formData: [
                { key: 'value' }
            ]
        });

        uploader.bind('afteraddingall', function (event, items) {

            $scope.newFileName = items[0].file.name;
        });

        uploader.bind('progress', function (event, item, progress) {
            $scope.progress = progress;
        });


        uploader.bind('completeall', function () {
            console.log('upload completed');
            Dialogs.showOKDialog('Upload erfolgreich', 'Achtung: Hochgeladene Dateien auf dem Medienserver sind je nach Dateigröße erst nach einiger Zeit erreichbar (Bis zu einer Stunde)');
            $scope.progress = 0;
        });


        $scope.canUpload = function () {
            if ($scope.newFileName !== null && $scope.newFileName !== '' && uploader.queue.length > 0) {
                return true;
            }
            else {
                return false;
            }
        };
        $scope.uploadNewFile = function () {
            uploader.queue[0].url = Security.getSimpleProtectedAdminUrlForEntryWithId('FTP', $scope.newFileName + '__.json');
            uploader.url = Security.getSimpleProtectedAdminUrlForEntryWithId('FTP', $scope.newFileName + '__.json');
            console.log(uploader);
            uploader.uploadAll();
            return;


        };


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);

    }]).
    controller('FtpDetailsCtrl', ['$scope', 'Ftp', '$routeParams', 'SimpleGenerator', 'Globals', '$fileUploader', 'Security', 'Dialogs', function ($scope, Ftp, $routeParams, SimpleGenerator, Globals, $fileUploader, Security, Dialogs) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'MedienServer-Datei',
            viewTitleField: 'name',
            id: $routeParams.id,
            restService: Ftp,
            restServiceDetailsFuncGet: function (params) {


                return Ftp.get(params, function (value) {

                    var ftpFileAddStaticImageUrl = function (resultglobals, ftpFile) {
                        ftpFile.imageUrlStatic = resultglobals.result[0].imageBaseUrl + ftpFile.name;
                    };

                    Globals.globalspromise.then(function (resultglobals) {
                        for (var index in value.result[0].list) {
                            var ftpFile = value.result[0].list[index];
                            ftpFileAddStaticImageUrl(resultglobals, ftpFile);

                        }
                    });

                });
            }
        });


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);


        var uploader = $scope.uploader = $fileUploader.create({
            scope: $scope,
            url: Security.getSimpleProtectedAdminUrlForEntryWithId('FTP', $scope.detailViewParams.id),
            formData: [
                { key: 'value' }
            ]
        });

        uploader.bind('afteraddingall', function () {

            $scope.newFileName = $routeParams.id;
            console.log('asd' + $scope.newFileName);

        });

        uploader.bind('progress', function (event, item, progress) {
            $scope.progress = progress;
        });


        uploader.bind('completeall', function () {
            console.log('upload completed');
            Dialogs.showOKDialog('Upload erfolgreich', 'Achtung: Hochgeladene Dateien auf dem Medienserver sind je nach Dateigröße erst nach einiger Zeit erreichbar (Bis zu einer Stunde)');
            $scope.progress = 0;
        });


        $scope.canUpload = function () {
            if ($scope.newFileName !== null && $scope.newFileName !== '' && uploader.queue.length > 0) {
                return true;
            }
            else {
                return false;
            }
        };
        $scope.uploadNewFile = function () {

            uploader.uploadAll();
            return;


        };

    }]);
