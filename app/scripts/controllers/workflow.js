'use strict';

angular.module('cubeAdminApp')
    .controller('WorkflowCtrl', ['$scope', 'Workflow', 'SimpleGenerator', function ($scope, Workflow, SimpleGenerator) {


        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Workflows',
            tableName: 'workflow',
            columns: [

                { prop: 'metaData', title: 'Erstellt am', type: 'htmlfiltered', propFilterHidden: true, transform: function (input) {
                    return input.lastChangedTimestamp + ' (' + input.lastChangedTimestamp + ')';
                }},
                { prop: 'workflowName', title: 'Name' },
                { prop: 'workflowState', title: 'Zustand' },
                { prop: 'currentState', title: 'Aktueller Schritt', type: 'htmlfiltered', propFilterHidden: true, transform: function (input) {
                    return input.activityName + ' (' + input.workflowStateEnum + ')';
                }}

            ],
            sort: 'metaData.lastChangedTimestamp',
            direction: 'DESC',
            restService: Workflow
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);
    }]).
    controller('WorkflowDetailsCtrl', ['$scope', 'Workflow', '$routeParams', 'SimpleGenerator', 'Enums', function ($scope, Workflow, $routeParams, SimpleGenerator, Enums) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Workflow',
            viewTitleField: 'id',
            id: $routeParams.id,
            restService: Workflow
        });

        $scope.states = Enums.getWorkflowStates();


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);

