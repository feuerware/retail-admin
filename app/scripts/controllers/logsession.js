'use strict';

/**
 * @ngdoc function
 * @name cubeAdminApp.controller:LogsessionCtrl
 * @description
 * # LogsessionCtrl
 * Controller of the cubeAdminApp
 */
angular.module('cubeAdminApp')
    .controller('LogSessionCtrl', ['$scope', 'LogSession', 'SimpleGenerator', 'Globals', function ($scope, LogSession, SimpleGenerator) {


        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'LogSessions',
            tableName: 'logsession',
            showCreateDefaultButton: false,
            sort: 'metaData.lastChangedTimestamp',
            direction: 'DESC',
            columns: [
                { prop: 'appId', title: 'AppId' },
                { prop: 'appVersion', title: 'AppVersion' },
                { prop: 'metaData', title: 'Datum', type: 'htmlfiltered', propFilter: 'creationTimestamp', propFilterHidden: true, transform: function (value) {
                    return value.lastChangedTimestamp;
                }},
                { prop: 'events', title: 'Events', type: 'htmlfiltered', transform: function (value) {

                    var output = '';
                    value.forEach(function (entry) {
                        output += entry.key + '<br>';
                    });
                    return output;
                }}
            ],
            restService: LogSession,
            restServiceFuncGet: function (params) {


                return LogSession.get(params, function () {

                });
            }
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);
    }]).
    controller('LogSessionDetailsCtrl', ['$scope', 'LogSession', '$routeParams', 'SimpleGenerator', 'Dialogs', function ($scope, LogSession, $routeParams, SimpleGenerator) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'LogSession',
            viewTitleField: 'id',
            id: $routeParams.id,
            restService: LogSession
        });


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);

