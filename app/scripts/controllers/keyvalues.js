'use strict';

angular.module('cubeAdminApp')
    .controller('KeyvaluesCtrl', ['$scope', 'KeyValues', 'SimpleGenerator', function ($scope, KeyValues, SimpleGenerator) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'KeyValues',
            viewTitleField: 'name',
            restService: KeyValues
        });


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);
