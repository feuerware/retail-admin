'use strict';

angular.module('cubeAdminApp')
    .controller('UserCtrl', ['$scope', 'User', 'SimpleGenerator', function UserCtrl($scope, User, SimpleGenerator) {

        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Kunden',
            tableName: 'user',
            columns: [
                { prop: 'lastName', title: 'Name' },
                { prop: 'firstName', title: 'Vorname' },
                { prop: 'customerID', title: 'AAX CustomerId' },
                { prop: 'active', title: 'Aktiv', type: 'boolean'  },
                { prop: 'email', title: 'Email' },
                { prop: 'mobilePhoneNumber', title: 'Telefon' }
            ],
            sort: 'metaData.lastChangedTimestamp',
            direction: 'DESC',
            restService: User
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);

    }]).
    controller('UserDetailsCtrl', ['$scope', 'User', '$routeParams', 'SimpleGenerator', 'Enums', 'Order', 'Basket', 'Suggestion', 'Note', 'Workflow', 'AppDevice', function ($scope, User, $routeParams, SimpleGenerator, Enums, Order, Basket, Suggestion, Note, Workflow, AppDevice) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Kunde',
            viewTitleField: 'lastName',
            id: $routeParams.id,
            restService: User,
            restServiceDetailsFuncGet: function (params) {
                // For requesting a single Object, rename "id" to "singleId" due to conflicts with tableview
                // params["singleId"] = params["id"];


                return User.get(params, function (value) {

                    var user = value.result[0];
                    Order.get({userId: user.id, sort: 'creationTime', direction: 'DESC'}, function (value) {
                        $scope.orders = value.result[0].list;
                    });


                    $scope.basket = Basket.get({userId: user.id}, function (value) {
                        if (value.result[0].listHeader.listSize > 0) {

                            $scope.basket = value.result[0].list[0];
                        }
                    });

                    Suggestion.get({receiverEmail: user.email, sort: 'metaData.creationTimestamp', direction: 'DESC'}, function (value) {
                        if (value.result[0].listHeader.listSize > 0) {
                            $scope.receivedSuggestions = value.result[0].list;
                        }
                    });


                    Suggestion.get({'sender.$id': user.id, sort: 'metaData.creationTimestamp', direction: 'DESC'}, function (value) {
                        if (value.result[0].listHeader.listSize > 0) {
                            $scope.sentSuggestions = value.result[0].list;
                        }
                    });

                    Note.get({userId: user.id, sort: 'creationTime', direction: 'DESC'}, function (value) {
                        if (value.result[0].listHeader.listSize > 0) {
                            $scope.note = value.result[0].list[0];
                        }
                    });

                    Workflow.get({'currentState.contextData.USER_ID': user.id, sort: 'metaData.creationTimestamp', direction: 'DESC'
                    }, function (value) {
                        if (value.result[0].listHeader.listSize > 0) {
                            $scope.workflows = value.result[0].list;
                        }
                    });

                    AppDevice.get({userId: user.id}, function (value) {
                        if (value.result[0].listHeader.listSize > 0) {
                            $scope.appDevices = value.result[0].list;
                        }
                    });

                });
            }

        })
        ;

        $scope.getOrderState = function (v) {
            return Enums.getTitleByValue(Enums.getOrderStates(), v);
        };
        $scope.getRole = function (v) {
            return Enums.getTitleByValue(Enums.getUserRoles(), v);
        };
        $scope.roles = Enums.getUserRoles();
        $scope.salutations = Enums.getSalutations();

        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);


    }])
;

