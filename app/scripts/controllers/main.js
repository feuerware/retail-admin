'use strict';

angular.module('cubeAdminApp')
    .controller('MainCtrl', ['Statistics', '$scope', function (Statistics, $scope) {

        /* Buggy stuff, commented out until clear what it shall do!
         Statistics.get({id: 'Order'}, function (value) {
         var map = value.result[0].list[0] || [];
         var array = [];


         for (var i in map) {
         array.push({key: i, value: map[i]});
         }

         array.sort(function (a, b) {
         return -(a.value - b.value);
         });

         $scope.customersWithOrder = array.length;
         $scope.orders = array;
         //User.get({id:})
         });
         */

        Statistics.get({}, function (value) {
            $scope.data = value.result[0];
            $scope.total = $scope.data.statisticGroups[0];
            $scope.daily = $scope.data.statisticGroups.slice(1);
            $scope.getTotalByKey = function (key) {
                for (var index in $scope.total.entries) {
                    var entry = $scope.total.entries[index];

                    if (entry.key === key) {
                        return entry;
                    }
                }
            };

            $scope.getTodayByKey = function (key) {

                for (var index in $scope.daily[0].entries) {
                    var entry = $scope.daily[0].entries[index];

                    if (entry.key.replace(/#.*/i, '') === key) {
                        return entry;
                    }
                }
            };

            $scope.getPerDayByKeyAndDay = function (key) {
                for (var index in $scope.data.daily) {

                    var entry = $scope.data.daily[index];
                    if (angular.isDefined(entry) && angular.isDefined(entry.entries)) {
                        for (var innerIndex in entry.entries) {
                            var entryInner = entry.entries[innerIndex];

                            if (entryInner.key === key) {
                                return entry;
                            }
                        }
                    }
                }
            };

            $scope.cleanName = function (name) {
                var cleanNamed = name.replace(/#.*/i, '');

                var map = {
                    'AppDevice': 'Geräte',
                    'Basket': 'Warenkörbe',
                    'Client': 'Mandanten',
                    'QueueEmail': 'Emails',
                    'ProductCategory': 'Kategorien',
                    'KeyValueGroup': 'Konstanten',
                    'Note': 'Merklisten',
                    'Notification': 'Benachrichtigungen',
                    'Order': 'Bestellungen',
                    'PickupStation': 'Abholstationen',
                    'Product': 'Produkte',
                    'QrCode': 'QrCodes',
                    'Retailer': 'Anbieter',
                    'Suggestion': 'Empfehlungen',
                    'User': 'Benutzer',
                    'WorkflowInstance': 'Aktive Workflows'
                };
                return map[cleanNamed];
            };
        });
    }]);
