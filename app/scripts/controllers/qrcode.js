'use strict';

angular.module('cubeAdminApp').
    controller('QrCodeCtrl', ['$scope', 'QrCode', 'Product', 'SimpleGenerator', function ($scope, QrCode, Product, SimpleGenerator) {


        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'QR-Codes',
            tableName: 'qrcode',
            columns: [
                { prop: 'qrCodeValue', title: 'Value' },
                { prop: 'product', title: 'Product', type: 'htmlfiltered', filter: 'ProductShort', propFilterHidden: true },
                { prop: 'qrCodeValue', title: 'Code', type: 'htmlfiltered', filter: 'QrCodeGenerator', propFilterHidden: true }


            ],
            restService: QrCode,
            restServiceFuncGet: function (params) {

                return QrCode.get(params, function (value) {


                    var getProductFromQRCodeFunction = function (qrcode) {
                        Product.get({id: qrcode.productID}, function (resultproduct) {

                            qrcode.product = resultproduct.result[0];
                        });
                    };

                    for (var index in value.result[0].list) {

                        var qrcode = value.result[0].list[index];

                        getProductFromQRCodeFunction(qrcode);


                    }


                });
            }
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);
    }]).
    controller('QrCodeDetailsCtrl', ['$scope', 'QrCode', '$routeParams', 'SimpleGenerator', function ($scope, QrCode, $routeParams, SimpleGenerator) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'QrCode',
            viewTitleField: 'id',
            id: $routeParams.id,
            restService: QrCode
        });


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);
