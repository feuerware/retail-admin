'use strict';

/**
 * @ngdoc function
 * @name cubeAdminApp.controller:PropertycontrollerCtrl
 * @description
 * # PropertycontrollerCtrl
 * Controller of the cubeAdminApp
 */
angular.module('cubeAdminApp')
    .controller('ApplicationPropertyCtrl', ['$scope', 'ApplicationProperty', 'SimpleGenerator', function ($scope, ApplicationProperty, SimpleGenerator) {

        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'ApplicationProperties',
            tableName: 'applicationproperty',
            showCreateDefaultButton: true,
            columns: [
                { prop: 'key', title: 'Key' },
                { prop: 'value', title: 'Value' }

            ],
            sort: 'metaData.lastChangedTimestamp',
            direction: 'DESC',
            restService: ApplicationProperty
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);
    }]).controller('ApplicationPropertyDetailsCtrl', ['$scope', 'ApplicationProperty', '$routeParams', 'SimpleGenerator', function ($scope, ApplicationProperty, $routeParams, SimpleGenerator) {

        console.log('Details Controller erzeugt');

        // Initialise Local List of Objectified RetailerIds
        $scope.currentTemplates = [];
        $scope.currentStartPages = [];
        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'ApplicationProperty',
            viewTitleField: 'name',
            id: $routeParams.id,
            restService: ApplicationProperty});


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);
    }]);
