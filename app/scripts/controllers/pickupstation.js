'use strict';

angular.module('cubeAdminApp')
    .controller('PickupstationCtrl', ['$scope', 'Pickupstation', 'SimpleGenerator', function ($scope, Pickupstation, SimpleGenerator) {

        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Abholstationen',
            tableName: 'pickupstation',
            showCreateDefaultButton: true,
            columns: [
                { prop: 'name', title: 'Name' },
                { prop: 'description', title: 'Beschreibung' },
                { prop: 'active', title: 'Aktiv', type: 'boolean'  }
                /*,
                 { prop: 'buildingAddress', title: 'Adresse', type: 'htmlfiltered', filter: 'BuildingAddressShort'  }*/
            ],
            restService: Pickupstation
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);


    }]).
    controller('PickupstationDetailsCtrl', ['$scope', 'Pickupstation', '$routeParams', 'SimpleGenerator', function ($scope, Pickupstation, $routeParams, SimpleGenerator) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Abholstation',
            viewTitleField: 'name',
            id: $routeParams.id,
            restService: Pickupstation
        });


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);