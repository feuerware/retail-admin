'use strict';

angular.module('cubeAdminApp')
    .controller('ProductCtrl', ['$scope', 'Product', 'SimpleGenerator', function ($scope, Product, SimpleGenerator) {


        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Produkte',
            tableName: 'product',
            showCreateDefaultButton: true,
            columns: [
                { prop: 'name', title: 'Name' },
                { prop: 'available', title: 'Verfügbar', type: 'boolean' },
                { prop: 'offer', title: 'Angebot', type: 'boolean'  },
                { prop: 'recommendation', title: 'Empfehlung', type: 'boolean'  },
                { prop: 'pictures', title: 'Bild', type: 'picarray' }
            ],
            restService: Product,
            restServiceFuncGet: Product.get
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);

    }]).
    controller('ProductDetailsCtrl', ['$scope', 'Product', '$routeParams', 'SimpleGenerator', 'Enums', 'Retailer', '$q', 'ProductCategory', 'Utility', function ($scope, Product, $routeParams, SimpleGenerator, Enums, Retailer, $q, ProductCategory, Utility) {


        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Produkt',
            viewTitleField: 'name',
            id: $routeParams.id,
            restService: Product,
            restServiceDetailsFuncGet: function (params) {
                // For requesting a single Object, rename "id" to "singleId" due to conflicts with tableview
                // params["singleId"] = params["id"];
                return Product.get(params, function (value) {
                    var p = value.result[0];


                    return p;

                });
            }
        });

        // Assign typeahead utility functions from services
        $scope.possibleOrderTypes = Enums.getPossibleOrderTypes();
        $scope.findRetailers = Retailer.findRetailers;
        $scope.findProductCategory = ProductCategory.findProductCategory;
        $scope.appendItemToArray = Utility.appendItemToArray;

        $scope.removeItemFromArray = Utility.removeItemFromArray;

        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);
