'use strict';

angular.module('cubeAdminApp')
    .controller('MailsCtrl', ['$scope', 'Mails', 'SimpleGenerator', function ($scope, Mails, SimpleGenerator) {


        $scope.tableViewParams = SimpleGenerator.createSafeTableViewParams({
            tableTitle: 'Mails',
            tableName: 'mails',
            columns: [
                { prop: 'emailData', title: 'Empfänger', type: 'htmlfiltered', propFilter: 'emailData.to', propFilterHidden: false, transform: function (value) {
                    return value.to;
                } },
                { prop: 'emailData', title: 'Betreff', type: 'htmlfiltered', propFilter: 'emailData.subject', transform: function (value) {
                    return value.subject;
                } },
                { prop: 'metaData', title: 'Datum', type: 'htmlfiltered', filter: 'date:\'dd.MM.yyyy HH:mm:ss\'', propFilter: 'creationTime', propFilterHidden: true, transform: function (value) {
                    return value.lastChangedTimestamp;
                }},
                { prop: 'status', title: 'Status' },

                { prop: 'retryCount', title: 'Wiederholungen' }

            ],
            sort: 'metaData.lastChangedTimestamp',
            direction: 'DESC',
            restService: Mails
        });


        $scope.tableParams = SimpleGenerator.getDefaultTableParams($scope.tableViewParams);
    }]).
    controller('MailDetailsCtrl', ['$scope', 'Mails', '$routeParams', 'SimpleGenerator', 'Dialogs', function ($scope, Mails, $routeParams, SimpleGenerator, Dialogs) {

        $scope.detailViewParams = SimpleGenerator.createSafeDetailViewParams({
            viewTitle: 'Mail',
            viewTitleField: 'id',
            id: $routeParams.id,
            restService: Mails
        });


        $scope.editorOptions = {
            language: 'de',
            uiColor: '#000000'
        };

        $scope.resend = function (data) {
            $scope.data.status = 'NEW';
            var params = { id: $scope.detailViewParams.id};

            $scope.detailViewParams.restService.post(params, data);
            Dialogs.showOKDialog('Änderung erfolgreich', 'Email wird in Kürze erneut versandt');
        };


        SimpleGenerator.requestDetailPageData($scope.detailViewParams, $scope);

    }]);
