'use strict';

/**
 * @ngdoc function
 * @name cubeAdminApp.controller:StatisticsCtrl
 * @description
 * # StatisticsCtrl
 * Controller of the cubeAdminApp
 */
angular.module('cubeAdminApp')
    .controller('StatisticsUserCtrl', ['$scope', 'Order', 'User', function ($scope, Order, User) {

        $scope.reportFileName = 'orders';
        $scope.debugText = '';

        $scope.downloadAsFile = function () {
            var csvString = $scope.csvOrders;
            var a = document.createElement('a');
            a.href = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csvString);
            a.target = '_blank';
            a.download = $scope.reportFileName + '.csv';

            var csvWin = window.open('', '', '');
            csvWin.document.write('<meta name="content-type" content="text/csv; charset=utf-8">');
            csvWin.document.write('<meta name="content-disposition" content="attachment;  filename=' + $scope.reportFileName + '.csv">  ');
            csvWin.document.write(csvString);
            document.body.appendChild(a);
            a.click();
        };


        $scope.configMarketingYes = {
            'labels': false,
            'title': 'Verteilung: Marketingklausel',
            'legend': {
                'display': true,
                'position': 'right'
            },
            'innerRadius': 0,
            'lineLegend': 'traditional'
        }
        ;


        $scope.configMarketingYesData = {
            'series': [
                'JA', 'NEIN'
            ],
            data: [
                {'x': 'JA', 'y': [1]},

                {'x': 'NEIN', 'y': [1]}

            ],
            temp: [0, 0],
            reset: function () {
                for (var index = 0; index < this.data.length; index++) {
                    this.temp[index] = 0;
                }
            },
            addEntryByAccepted: function (yes) {

                var index = 0;
                if (yes) {
                    index = 0;
                } else {
                    index = 1;
                }

                this.temp[index]++;
            },
            updateUI: function () {
                for (var index = 0; index < this.temp.length; index++) {
                    this.data[index].y[0] = this.temp[index];
                }
            }


        };


        $scope.calculateContactOverview = function () {
            console.log('a');
            $scope.loadingUser = true;
            var params = {};
            params.direction = 'DESC';
            params.limit = 999999;
            params.page = 0;
            params.sort = 'creationTime';

            $scope.allMailsAsString = '';
            $scope.configMarketingYesData.reset();
            $scope.userOverviewLoadedOnce = false;

            User.get(params).$promise.then(
                //success
                function (value) {
                    $scope.csvOrders = '';
                    $scope.toCountUser = 0;
                    var firstLine = 'Salutation;FirstName;LastName;Email\n';
                    var csvTemp = firstLine;

                    $scope.debugText = 'Berechne Kunden mit Einwilligungsklausel\n';

                    $scope.countNotAccepted = 0;
                    $scope.countAccepted = 0;

                    for (var index = 0; index < value.result[0].list.length; index++) {


                        var user = value.result[0].list[index];
                        var hasProblem = false;

                        if (!angular.isDefined(user.firstName)) {
                            user.firstName = 'unbekannt';
                            hasProblem = true;
                        }

                        if (!angular.isDefined(user.lastName)) {
                            user.lastName = 'unbekannt';
                            hasProblem = true;
                        }

                        if (!angular.isDefined(user.email)) {
                            user.email = 'unbekannt';
                            hasProblem = true;
                        }


                        if (!angular.isDefined(user.salutation)) {
                            user.salutation = 'unbekannt';
                            hasProblem = true;
                        } else {
                            user.salutation = (user.salutation === 'MALE' ? 'Herr' : 'Frau');
                        }


                        var allows = false;
                        if (angular.isDefined(user.flags) && angular.isDefined(user.flags.marketingContact)) {
                            allows = true;
                        }
                        var line = user.salutation + ';' + user.firstName + ';' + user.lastName + ';' + user.email;


                        if (allows) {
                            csvTemp += line + '\n';
                            $scope.countAccepted++;
                            if (hasProblem) {
                                $scope.debugText += 'Problem bei Kunde: ' + user.salutation + ';' + user.firstName + ';' + user.lastName + ';' + user.email + '\n';

                            }

                            $scope.allMailsAsString += user.email + ';';
                        } else {
                            $scope.countNotAccepted++;
                        }


                        $scope.configMarketingYesData.addEntryByAccepted(allows);


                    }


                    $scope.debugText += 'Anzahl Kunden mit "Marketingeinwilligungsklause akzeptiert": ' + $scope.countAccepted + '\n';
                    $scope.debugText += 'Anzahl Kunden "Marketingeinwilligungsklausel nicht akzeptiert": ' + $scope.countNotAccepted + '\n';

                    $scope.debugText += 'Berechnung abgeschlossen';

                    $scope.csvOrders = csvTemp;
                    $scope.csvOrdersLoadedOnce = true;
                    $scope.data = value.result[0];
                    $scope.loadingUser = false;
                    $scope.reportFileName = 'marketing_users';
                    $scope.configMarketingYesData.updateUI();

                },
                //error
                function () {
                    $scope.loadingUser = false;
                });
        };


        $scope.configOrderCountPerCustomer = {
            'labels': false,
            'title': 'Verteilung: Anzahl Bestellungen',
            'legend': {
                'display': true,
                'position': 'right'
            },
            'innerRadius': 0,
            'lineLegend': 'traditional'
        };


        $scope.configOrderCountPerCustomerData = {
            'series': [
                '0', '1', '2-5', '6-10', '> 10'
            ],
            data: [
                {'x': '0', 'y': [1]},
                {'x': '1', 'y': [1]},
                {'x': '2-5', 'y': [1]},
                {'x': '6-10', 'y': [1]},
                {'x': '> 10', 'y': [1]}
            ],
            temp: [0, 0, 0, 0, 0],
            reset: function () {
                for (var index = 0; index < this.data.length; index++) {
                    this.temp[index] = 0;
                }
            },
            addEntryByOrderCount: function (orderCount) {

                var index = 0;
                if (orderCount < 1) {
                    index = 0;
                } else if (orderCount < 2) {
                    index = 1;
                } else if (orderCount < 6) {
                    index = 2;
                } else if (orderCount < 11) {
                    index = 3;
                } else {
                    index = 4;
                }

                this.temp[index]++;
            },
            updateUI: function () {
                for (var index = 0; index < this.temp.length; index++) {
                    this.data[index].y[0] = this.temp[index];
                }
            }


        };


        $scope.calculateUserOverview = function () {
            console.log('a');
            $scope.loadingUserOverview = true;
            var params = {};
            params.direction = 'DESC';
            params.limit = 999999;
            params.page = 0;
            params.sort = 'creationTime';
            $scope.countAccepted = 0;
            $scope.configMarketingYesData.reset();
            $scope.configOrderCountPerCustomerData.reset();
            $scope.csvOrdersLoadedOnce = false;

            User.get(params).$promise.then(
                //success
                function (value) {
                    $scope.csvOrders = '';
                    $scope.toCountUserOverview = 0;

                    $scope.countNotAccepted = 0;
                    $scope.countAccepted = 0;


                    var userStatisticsFunction = function (user) {
                        var userParams = {};
                        userParams.limit = 1;
                        userParams.userId = user.id;
                        Order.get(userParams).$promise.then(function (value) {
                                var orderCount = value.result[0].listHeader.listSize;
                                $scope.configOrderCountPerCustomerData.addEntryByOrderCount(orderCount);

                                $scope.toCountUserOverview--;
                                if ($scope.toCountUserOverview === 0) {
                                    $scope.configMarketingYesData.updateUI();
                                    $scope.configOrderCountPerCustomerData.updateUI();
                                    $scope.loadingUserOverview = false;
                                    $scope.userOverviewLoadedOnce = true;
                                }
                            },
                            // error
                            function () {
                                $scope.toCountUserOverview--;
                                if ($scope.toCountUserOverview === 0) {
                                    $scope.configMarketingYesData.updateUI();
                                    $scope.configOrderCountPerCustomerData.updateUI();
                                    $scope.loadingUserOverview = false;
                                    $scope.userOverviewLoadedOnce = true;

                                }

                            });
                    };

                    for (var index = 0; index < value.result[0].list.length; index++) {
                        $scope.toCountUserOverview++;

                        var user = value.result[0].list[index];

                        var allows = false;
                        if (angular.isDefined(user.flags) && angular.isDefined(user.flags.marketingContact)) {
                            allows = true;
                        }

                        if (allows) {
                            $scope.countAccepted++;
                        } else {
                            $scope.countNotAccepted++;
                        }


                        $scope.configMarketingYesData.addEntryByAccepted(allows);


                        userStatisticsFunction(user);


                    }


                },
                //error
                function () {
                    $scope.loadingUserOverview = false;
                });
        };


    }])
;