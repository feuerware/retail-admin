'use strict';

/**
 * @ngdoc directive
 * @name cubeAdminApp.directive:DesignSettingsEditor
 * @description
 * # DesignSettingsEditor
 */
angular.module('cubeAdminApp')
    .directive('designSettingsEditor', ['Helper', function (Helper) {
        return {
            templateUrl: 'views/directives/rt-designsettings.html',
            restrict: 'A',
            scope: {
                designSettings: '=designSettings'

            },
            link: function postLink($scope) {
                Helper.cleanBackendDesignSettings($scope.designSettings);
                $scope.hexToColor = Helper.hexToColor;
            }
        };
    }]);
