'use strict';

angular.module('cubeAdminApp')
    .directive('refreshButton', ['$route', function ($route) {
        return {
            restrict: 'A',
            link: function postLink(scope, element) {
                element.bind('click', function () {
                    $route.reload();
                    scope.$apply();
                });
            }
        };
    }]);
