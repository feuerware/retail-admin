'use strict';

angular.module('cubeAdminApp')
    .directive('backButton', function () {
        return {
            restrict: 'A',
            link: function postLink(scope, element) {
                element.bind('click', function () {
                    history.back();
                    scope.$apply();
                });
            }
        };
    });
