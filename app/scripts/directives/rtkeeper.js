'use strict';

angular.module('cubeAdminApp')
  .directive('rtKeeper',[ 'Keeper', function (Keeper) {
    return {
        templateUrl: 'views/directives/rt-keeper.html',
        restrict: 'A',
        scope: {
            // Reference to object with field that represents a retailerId (2way binding for editing)
            target: '='
        },
        link: function ($scope) {


            $scope.isLoading = true;

            console.log($scope.targetType);



            $scope.$watch('target', function (value) {
                if (value) {
                    $scope.loadKeeper();
                    $scope.targetType = value.metaData.type;
                }
                $scope.targetProps = angular.copy(value);
                console.log('change');
            });


            $scope.setKeeperInScope = function(keeper) {
                if(keeper) {
                    for (var prop in $scope.target) {

                        if (keeper.propertiesToKeep && keeper.propertiesToKeep.indexOf(prop) >= 0) {
                            $scope.targetProps[prop] = true;
                        } else {
                            $scope.targetProps[prop] = false;
                        }
                    }
                }

                $scope.keeper = keeper;
            };

            $scope.loadKeeper = function() {
                $scope.isLoading = true;


                Keeper.get({targetTypeName:$scope.targetType,targetId:$scope.target.id}, function (value) {

                    if (value.result[0].listHeader.listSize > 0) {
                        var p = value.result[0].list[0];
                        console.log(value);
                        $scope.setKeeperInScope(p);
                    } else {
                        $scope.keeper = null;
                    }




                    $scope.isLoading = false;

                });

            };


            $scope.createKeeper = function() {
                $scope.isLoading = true;


                Keeper.post({}, function (value) {
                    console.log(value);


                    if (value.result[0]) {
                        var p = value.result[0];
                        p.targetTypeName =$scope.targetType;
                        p.targetId = $scope.target.id;
                        p.deletionLock=true;
                        $scope.setKeeperInScope(p);
                        console.log('result');
                        console.log($scope.keeper);

                        $scope.saveKeeper();
                    } else {
                        $scope.keeper = null;
                    }

                    $scope.isLoading = false;

                });
            };


            $scope.deleteKeeper = function() {
                console.log($scope.keeper);
                Keeper.delete({ singleId:$scope.keeper.id}, function (value) {
                    console.log(value);
                    $scope.setKeeperInScope(null);
                    $scope.isLoading = false;

                });
            };

            $scope.saveKeeper = function() {
                $scope.isLoading = true;


                $scope.keeper.singleId = $scope.keeper.id;
                $scope.keeper.propertiesToKeep = [];

                for (var prop in $scope.target) {

                    if ($scope.targetProps[prop]) {
                        $scope.keeper.propertiesToKeep.push(prop);
                    }
                }

                Keeper.post($scope.keeper, function (value) {
                    console.log(value);

                    if (value.result[0]) {
                        $scope.setKeeperInScope(value.result[0]);
                    } else {
                        $scope.setKeeperInScope(null);
                    }

                    $scope.isLoading = false;

                });
            };




        }
    };
  }]);
