'use strict';

/**
 * @ngdoc directive
 * @name cubeAdminApp.directive:StartPageCategory
 * @description
 * # StartPageCategory
 */
angular.module('cubeAdminApp')
    .directive('StartPageCategory', function () {
        return {
            template: '<div></div>',
            restrict: 'E',
            link: function postLink(scope, element) {
                element.text('this is the StartPageCategory directive');
            }
        };
    });
