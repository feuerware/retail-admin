'use strict';

/**
 * @ngdoc directive
 * @name cubeAdminApp.directive:ApplicationProperty
 * @description
 * # ApplicationProperty
 */
angular.module('cubeAdminApp')
    .directive('rtApplicationProperty', ['ApplicationProperty', 'Dialogs', function (ApplicationProperty, Dialogs) {
        return {
            templateUrl: 'views/directives/rt-applicationproperty.html',
            restrict: 'A',
            scope: {
                // The field name (remark: can be an index for an array as well!
                propertyName: '@propertyName',
                serverProperties: '=serverProperties'

            },
            link: function postLink(scope) {

                /**
                 * Method to create a non existant property in the database
                 * this is done through taking the current value and execute the createProperty command in the service
                 */
                scope.createProperty = function () {
                    var currentvalue = scope.serverProperties.list[0].config[scope.propertyName] || scope.serverProperties.list[0].default[scope.propertyName];
                    ApplicationProperty.createProperty(scope.propertyName, currentvalue).then(function (value) {
                        scope.property = value;

                    });

                };

                // Method to delete current property from database
                scope.deleteProperty = function () {
                    ApplicationProperty.delete({singleId: scope.property.id}, function (value) {

                        // on deletionremove from current js objects
                        delete  scope.serverProperties.list[0].live[scope.propertyName];
                        delete scope.property;
                    });
                };

                // method to store changed value in database
                scope.updateProperty = function () {
                    scope.property.singleId = scope.property.id;
                    ApplicationProperty.post(scope.property, function (value) {

                        // transfer value to current properties list
                        scope.serverProperties.list[0].live[scope.propertyName] = scope.property.value;
                    });
                };

                // determine if it is an editable property
                scope.editable = !scope.serverProperties.list[0].unchangeable[scope.propertyName];

                // If live property exists, expose object, retrieve it through ApplicationProperty service
                if (scope.serverProperties.list[0].live[scope.propertyName]) {
                    ApplicationProperty.get({singleId: scope.propertyName.replace(/\./g, '#')}, function (value) {
                            scope.property = value.result[0];

                        }
                    );
                }

            }
        };
    }]);
