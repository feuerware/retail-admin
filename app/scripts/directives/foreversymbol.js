'use strict';

angular.module('cubeAdminApp')
    .directive('foreversymbol', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.addClass('glyphicon');
                element.addClass('glyphicon-floppy-disk');
            }
        };
    });