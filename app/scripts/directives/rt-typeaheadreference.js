'use strict';

/**
 * @ngdoc directive edit field for a retailer link
 * @name cubeAdminApp.directive:RetailerEditDirective
 * @description
 * # RetailerEditDirective                  rtTypeaheadReference
 * use this wherever a retailer is linked via an id
 */
angular.module('cubeAdminApp')
    .directive('rtTypeaheadReference', [ function () {
        return {
            templateUrl: 'views/directives/rt-typeaheadreference.html',
            restrict: 'A',
            scope: {
                // Reference to object with field that represents a retailerId (2way binding for editing)
                object: '=object',
                // The field name (remark: can be an index for an array as well!
                field: '@field',
                searchMethod: '&searchMethod'
            },
            link: function ($scope) {
                $scope.items = [];
                // Helper function to set back value to assigned model
                $scope.storeIdToModel = function ($item, $model) {
                    $scope.object[$scope.field] = $model.id;
                };

                // Get all elements for selectbox
                $scope.searchMethod({object: {}}).then(function (value) {
                    $scope.items = value;
                });

                // check if object is already assigned
                $scope.$watch('object', function (value) {
                    // Initialisation method to load associated retailer
                    if (value) {
                        if ($scope.object[$scope.field]) {
                            $scope.searchMethod({object: {singleId: $scope.object[$scope.field]}}).then(function (value) {
                                $scope.currentItem = value[0];
                            });
                        }
                    }
                });


            }
        };
    }]);
