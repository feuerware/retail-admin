'use strict';

(function (ng) {

    if (!document.URL.match(/nobackend/)) {
        return; // do nothing special - this app is not gonna use stubbed backend
    }


    function initializeStubbedBackend() {
        // 'codebrag' is our main application module
        ng.module('cubeAdminApp')
            .config(function ($provide) {
                $provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator);
            })
            .run(function ($rootScope, $httpBackend, Helper, MockProducts, MockServerStatus, MockPickupStation, MockUser) {

                $httpBackend.whenGET(/views\/\w+.*/).passThrough();
                MockProducts.addStubs($httpBackend);
                MockServerStatus.addStubs($httpBackend);
                MockPickupStation.addStubs($httpBackend);
                MockUser.addStubs($httpBackend);

                $rootScope.isDemoMode = true;
                Helper.setIsDemoMode(true);
            });
    }

    console.log('======== USING STUBBED BACKEND ========');
    initializeStubbedBackend();


})(angular);