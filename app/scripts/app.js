'use strict';

angular.module('cubeAdminApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ngModal',
    'ngTable',
    'ui.bootstrap',
    'ngCkeditor',
    'aInterceptor',
    'angularFileUpload',
    'ngStorage',
    'angularCharts'
]).config(function ($routeProvider, $locationProvider, $logProvider) {

    $logProvider.debugEnabled(false);
    $locationProvider.html5Mode(false);
    $routeProvider
        .when('/', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
        })
        .when('/mails', {
            templateUrl: 'views/simpletable.html',
            controller: 'MailsCtrl'
        })
        .when('/mails/:id', {
            templateUrl: 'views/mails.html',
            controller: 'MailDetailsCtrl'
        })
        .when('/product', {
            templateUrl: 'views/simpletable.html',
            controller: 'ProductCtrl'
        })
        .when('/product/:id', {
            templateUrl: 'views/product.html',
            controller: 'ProductDetailsCtrl'
        })
        .when('/serverstatus', {
            templateUrl: 'views/serverstatus.html',
            controller: 'ServerstatusCtrl'
        })
        .when('/pickupstation', {
            templateUrl: 'views/simpletable.html',
            controller: 'PickupstationCtrl'
        })
        .when('/pickupstation/:id', {
            templateUrl: 'views/pickupstation.html',
            controller: 'PickupstationDetailsCtrl'
        })
        .when('/user', {
            templateUrl: 'views/simpletable.html',
            controller: 'UserCtrl'
        })
        .when('/user/:id', {
            templateUrl: 'views/user.html',
            controller: 'UserDetailsCtrl'
        })
        .when('/order', {
            templateUrl: 'views/simpletable.html',
            controller: 'OrderCtrl'
        }).when('/order/:id', {
            templateUrl: 'views/order.html',
            controller: 'OrderDetailsCtrl'
        })
        .when('/productcategory', {
            templateUrl: 'views/simpletable.html',
            controller: 'ProductCategoryCtrl'
        })
        .when('/productcategory/:id', {
            templateUrl: 'views/productcategory.html',
            controller: 'ProductCategoryDetailsCtrl'
        })
        .when('/client', {
            templateUrl: 'views/simpletable.html',
            controller: 'ClientCtrl'
        })
        .when('/client/:id', {
            templateUrl: 'views/client.html',
            controller: 'ClientDetailsCtrl'
        })
        .when('/retailer', {
            templateUrl: 'views/simpletable.html',
            controller: 'RetailerCtrl'
        })
        .when('/retailer/:id', {
            templateUrl: 'views/retailer.html',
            controller: 'RetailerDetailsCtrl'
        })
        .when('/workflow', {
            templateUrl: 'views/simpletable.html',
            controller: 'WorkflowCtrl'
        })
        .when('/workflow/:id', {
            templateUrl: 'views/workflow.html',
            controller: 'WorkflowDetailsCtrl'
        })
        .when('/qrcode', {
            templateUrl: 'views/simpletable.html',
            controller: 'QrCodeCtrl'
        })
        .when('/qrcode/:id', {
            templateUrl: 'views/qrcode.html',
            controller: 'QrCodeDetailsCtrl'
        })
        .when('/keyvalues', {
            templateUrl: 'views/keyvalues.html',
            controller: 'KeyvaluesCtrl'
        })
        .when('/ftp', {
            templateUrl: 'views/ftplist.html',
            controller: 'FtpCtrl'
        })
        .when('/ftp/:id', {
            templateUrl: 'views/ftp.html',
            controller: 'FtpDetailsCtrl'
        })
        .when('/startpage', {
            templateUrl: 'views/simpletable.html',
            controller: 'StartPageCtrl'
        })
        .when('/startpage/:id', {
            templateUrl: 'views/startpage.html',
            controller: 'StartPageDetailsCtrl'
        })
        .when('/notification', {
            templateUrl: 'views/simpletable.html',
            controller: 'NotificationCtrl'
        })
        .when('/notification/:id', {
            templateUrl: 'views/notification.html',
            controller: 'NotificationDetailsCtrl'
        })
        .when('/appdevice', {
            templateUrl: 'views/simpletable.html',
            controller: 'AppDeviceCtrl'
        })
        .when('/appdevice/:id', {
            templateUrl: 'views/appdevice.html',
            controller: 'AppDeviceDetailsCtrl'
        })
        .when('/livelog', {
            templateUrl: 'views/livelog.html',
            controller: 'LiveLogCtrl'
        })
        .when('/template', {
            templateUrl: 'views/simpletable.html',
            controller: 'PageCtrl'
        })
        .when('/template/:id', {
            templateUrl: 'views/page.html',
            controller: 'PageDetailsCtrl'
        })
        .when('/statistics', {
            templateUrl: 'views/statistics.html',
            controller: 'StatisticsCtrl'
        })
        .when('/statisticsuser', {
            templateUrl: 'views/statisticsuser.html',
            controller: 'StatisticsUserCtrl'
        })
        .when('/corporate', {
            templateUrl: 'views/corporatelist.html',
            controller: 'CorporateCtrl'
        })
        .when('/corporate/:id', {
            templateUrl: 'views/corporate.html',
            controller: 'CorporateDetailsCtrl'
        })
        .when('/keeper', {
            templateUrl: 'views/simpletable.html',
            controller: 'KeeperCtrl'
        })
        .when('/keeper/:id', {
            templateUrl: 'views/keeper.html',
            controller: 'KeeperDetailsCtrl'
        }).when('/applicationproperty', {
            templateUrl: 'views/applicationpropertylist.html',
            controller: 'ApplicationPropertyCtrl'
        }).when('/applicationproperty/:id', {
            templateUrl: 'views/applicationproperty.html',
            controller: 'ApplicationPropertyDetailsCtrl'
        })
        .when('/logsession', {
            templateUrl: 'views/simpletable.html',
            controller: 'LogSessionCtrl'
        }).when('/logsession/:id', {
            templateUrl: 'views/logsession.html',
            controller: 'LogSessionDetailsCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });

});