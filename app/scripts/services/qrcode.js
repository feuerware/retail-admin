'use strict';

angular.module('cubeAdminApp')
    .service('QrCode', ['Security',
        function QrCode(Security) {
            return Security.getSimpleProtectedAdminResource('QrCode');
        }
    ]);