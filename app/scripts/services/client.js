'use strict';

angular.module('cubeAdminApp')
    .service('Client', ['Security',
        function Client(Security) {
            return Security.getSimpleProtectedAdminResource('Client');
        }
    ]);