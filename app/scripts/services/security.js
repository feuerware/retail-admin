'use strict';

angular.module('cubeAdminApp')
    .service('Security', ['$resource', '$localStorage', 'Helper', '$templateCache', '$cacheFactory', '$timeout', function Security($resource, $localStorage, Helper, $templateCache, $cacheFactory, $timeout) {

        this.getUrlForLogin = function () {
            return Helper.getApi() + '/Admin/Login';
        };
        this.getRes = function () {
            return $resource(this.getUrlForLogin(), {
                id: '@id'
            }, {
                'login': {
                    method: 'GET',
                    isArray: false
                }, 'createAdmin': {
                    method: 'POST',
                    isArray: false
                }
            });
        };


        this.getToken = function () {
            return $localStorage.token;
        };

        this.setToken = function (token) {
            $localStorage.token = token;
        };


        this.getLocalUser = function () {
            var email = $localStorage.email;
            var password = $localStorage.password;
            var token = $localStorage.token;
            if (email && password && token) {
                return this.getRes().login({email: email, password: password});
            }
            else {
                return null;
            }


        };

        this.isLoggedIn = function () {
            if ($localStorage.email !== undefined && $localStorage.password !== undefined) {
                return true;
            }
            else {
                return false;
            }
        };

        this.tryLogin = function (email, password) {
            return this.getRes().login({email: email, password: password});
        };

        this.savePassword = function (password) {
            $localStorage.password = password;
        };

        this.logout = function () {
            var mail = $localStorage.email;
            $localStorage.$reset({
                email: mail
            });
            $templateCache.removeAll();
            $cacheFactory.get('$http').removeAll();
        };

        this.saveEmail = function (value) {
            $localStorage.email = value;
        };
        this.getSavedEmail = function () {
            return $localStorage.email || '';
        };

        this.getSimpleProtectedAdminUrlForEntryWithId = function (name, id) {
            return Helper.getApi() + '/Admin/' + this.getToken() + '/' + name + '/' + id;
        };
        this.getSimpleProtectedAdminUrl = function (name) {
            return Helper.getApi() + '/Admin/' + this.getToken() + '/' + name;
        };

        // Default Admin Resource Handler
        this.getSimpleProtectedAdminResource = function (name) {
            // INFO: renamed "id" to "singleId" due to filtering issues in tableView
            var result = $resource(Helper.getApi() + '/Admin/' + this.getToken() + '/' + name + '/:singleId', {
                singleId: '@singleId'
            }, {
                'get': {
                    method: 'GET',
                    isArray: false
                },
                'post': {
                    method: 'POST',
                    isArray: false
                },
                'delete': {
                    method: 'DELETE',
                    isArray: false
                }
            });

            return result;

        };

        // resource handler with Dialogs output for delte and post operations
        this.getSimpleProtectedAdminResourceWithDialogs = function (Dialogs, Messages, name) {

            // Rewrite result to include messages to the user on POST requests and stuff
            // fixme: reuse the standard resource created in same function without dialogs
            var result = $resource(Helper.getApi() + '/Admin/' + this.getToken() + '/' + name + '/:singleId', {
                singleId: '@singleId'
            }, {
                'get': {
                    method: 'GET',
                    isArray: false
                },
                'post': {
                    method: 'POST',
                    isArray: false
                },
                'delete': {
                    method: 'DELETE',
                    isArray: false
                }
            });

            // Just log POST Result
            result.originalPost = result.post;

            // override any post request
            result.post = function (params) {

                var message = Messages.getUserMessage(name, params);

                Dialogs.showYESNODialog(message.title, message.message,
                    function (yesnoresult) {
                        if (yesnoresult) {
                            result.originalPost(params, function (result) {

                                if (result.info.statusCode === 100) {

                                    Dialogs.showOKDialog(message.title, message.success);
                                } else {
                                    Dialogs.showOKDialog('FEHLER' + message.title, message.fail + result.info.status + '/' + result.info.statusCode);

                                }
                            }, function error(value) {
                                Dialogs.showOKDialog('SCHWERER FEHLER' + message.title, message.fail + value.status);


                            });
                        }

                    }
                )
                ;


            };

            return result;
        };

        this.fullPageReload = function () {
            $timeout(function () {
                window.location.reload(true);
                console.log('hard reload');
            }, 300);
        };

    }
    ])
;
