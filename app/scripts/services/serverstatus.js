'use strict';

angular.module('cubeAdminApp')
    .service('ServerStatus', ['Security',
        function ServerStatus(Security) {
            return Security.getSimpleProtectedAdminResource('Status');
        }
    ]);