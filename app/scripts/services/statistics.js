'use strict';

angular.module('cubeAdminApp')
    .service('Statistics', ['Security',
        function Statistics(Security) {
            return Security.getSimpleProtectedAdminResource('Statistics');
        }
    ]);