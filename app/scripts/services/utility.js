'use strict';

/**
 * @ngdoc service
 * @name cubeAdminApp.utility
 * @description
 * # utility
 * Service in the cubeAdminApp.
 *
 * Collects reusable utility methods for not having them globally defined!
 *
 */
angular.module('cubeAdminApp')
    .service('Utility', function () {
        // AngularJS will instantiate a singleton by calling "new" on this function

        var result = {};

        // Array Utilities
        result.appendItemToArray = function (array) {
            if (!array) {
                array = [];
            }
            array.push('');

        };

        result.removeItemFromArray = function (array, item) {

            array.splice(array.indexOf(item), 1);
        };


        return result;


    });
