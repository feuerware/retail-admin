'use strict';

Array.prototype.move = function (from, to) {
    this.splice(to, 0, this.splice(from, 1)[0]);
};

angular.module('cubeAdminApp')
    .service('Helper', [function Helper() {

        var currentServerDescription = null;
        var isDemoMode = false;

        this.apis = [
            {'address': 'http://127.0.0.1:8100/retail-api',
                'description': 'Localhost (http://127.0.0.1:8100/retail-api)',
                'status': 1},
            {'address': 'http://dev.feuerware.com:8100/retail-api',
                'description': 'Feuerware-Dev (http://dev.feuerware.com:8100/retail-api)',
                'status': 1},
            {'address': 'http://production.feuerware.com:8100/retail-api',
                'description': 'Feuerware-Prod (http://production.feuerware.com:8100/retail-api)',
                'status': 1},
            {'address': 'https://telematix.telekom.de/int/retail-api',
                'description': 'Int (https://telematix.telekom.de/int/retail-api)',
                'status': 1},
            {'address': 'https://telematix.telekom.de/demo/retail-api',
                'description': 'Demo (https://telematix.telekom.de/demo/retail-api)',
                'status': 1},
            {'address': 'https://q4de1csy02j.itenos.t-systems.com/mall2go/retail-api',
                'description': 'Tua (https://q4de1csy02j.itenos.t-systems.com/mall2go/retail-api)',
                'status': 1},
            {'address': 'https://q4de1csy02h.itenos.t-systems.com/mall2go/retail-api',
                'description': 'Wirk (https://q4de1csy02h.itenos.t-systems.com/mall2go/retail-api)',
                'status': 1}
        ];

        if (typeof apiSettings !== 'undefined' && apiSettings && apiSettings.api) {
            this.apis.unshift({'address': apiSettings.api,
                'description': 'local (' + apiSettings.api + ')',
                'status': 1});
            console.log('added element with api' + apiSettings.api);
        }


        this.getApis = function () {
            return this.apis;
        };

        this.selectHost = function (api) {
            localStorage.setItem('selectedHost', JSON.stringify(api));
            this.loadHostFromStorageOrUseDefault();
        };

        this.loadHostFromStorageOrUseDefault = function () {
            var host;
            var savedHost = localStorage.getItem('selectedHost');
            if (savedHost) {
                host = JSON.parse(savedHost);
            }
            if (!host) {
                host = this.apis[0];
            }
            currentServerDescription = host;

            return currentServerDescription;
        };

        this.getHost = function () {
            return currentServerDescription.address;
        };
        this.getApi = function () {
            return this.getHost();
        };


        this.setIsDemoMode = function (mode) {
            isDemoMode = mode;
        };

        this.getIsDemoMode = function () {
            return isDemoMode;
        };


        this.loadHostFromStorageOrUseDefault();


        this.isResponseOK = function (value) {
            if (value === undefined || value.result === undefined || value.info === undefined || value.info.status !== 'OK') {
                return false;
            } else {
                return true;
            }
        };

        this.isResponseOKAndShowDialogOtherwise = function (value) {

            var result = this.isResponseOK(value);
            if (!result) {
                this.showErrorDialogForResponse(value);
            }
            return result;
        };


        this.hexToColor = function (inputColor) {
            return inputColor && inputColor.replace('0x', '#');
        };

        this.cleanBackendDesignSettings = function (designSettings) {
            if (designSettings) {
                delete designSettings.firstColorString;
                delete designSettings.secondColorString;
                delete designSettings.textColorString;
                delete designSettings.thirdColorString;
                delete designSettings.keyColorString;
            }
        };


    }]);
