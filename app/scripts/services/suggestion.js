'use strict';

angular.module('cubeAdminApp')
    .service('Suggestion', ['Security',
        function Suggestion(Security) {
            return Security.getSimpleProtectedAdminResource('Suggestion');
        }
    ]);