'use strict';

angular.module('cubeAdminApp')
    .service('SimpleGenerator', ['$rootScope', 'ngTableParams', '$sessionStorage', 'Dialogs', '$route', '$location', function SimpleGenerator($rootScope, ngTableParams, $sessionStorage, Dialogs, $route, $location) {

        this.getDefaultTableParams = function (tableViewParams) {

            if (!angular.isDefined($sessionStorage.tablePage)) {
                $sessionStorage.tablePage = [];
            }
            if (!angular.isDefined($sessionStorage.tableCount)) {
                $sessionStorage.tableCount = [];
            }

            var savedPage = $sessionStorage.tablePage[tableViewParams.tableName] || 1;
            var savedCount = $sessionStorage.tableCount[tableViewParams.tableName] || 10;
            /* jshint -W055 */
            return new ngTableParams({
                /* jshint +W055 */
                page: savedPage,            // show first page
                count: savedCount,          // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                }, filter: { }
            }, {
                total: 0,           // length of data
                getData: function ($defer, params) {
                    console.log('GETDATA!', params);

                    var restParams = {limit: params.count(), page: params.page() - 1};
                    for (var attr in params.filter()) {
                        var val = params.filter()[attr];
                        if (val !== null && val !== '') {
                            restParams[attr] = val;
                        }
                    }


                    if (tableViewParams.sort) {
                        restParams.sort = tableViewParams.sort;
                    }

                    if (tableViewParams.direction) {
                        restParams.direction = tableViewParams.direction;
                    }


                    tableViewParams.restServiceFuncGet(restParams).$promise.then(
                        //success
                        function (value) {
                            console.log('restServiceFuncGet', value);
                            params.total(value.result[0].listHeader.listSize);
                            tableViewParams.total = value.result[0].listHeader.listSize;
                            tableViewParams.page = restParams.page + 1;
                            tableViewParams.data = value.result[0].list;
                            $defer.resolve(value.result[0].list);

                            $sessionStorage.tablePage[tableViewParams.tableName] = tableViewParams.page;
                            $sessionStorage.tableCount[tableViewParams.tableName] = params.count();
                        }
                    );
                }
            });
        };

        this.createSafeTableViewParams = function (viewParams) {
            var transformFunction = function (input) {
                return input;
            };

            for (var index in viewParams.columns) {
                var col = viewParams.columns[index];
                if (!col.propFilter) {
                    col.propFilter = col.prop;
                }
                if (col.type === 'htmlfiltered') {
                    if (!col.filter) {

                        col.filter = 'input';
                    }
                    if (!col.transform) {
                        col.transform = transformFunction;
                    }
                }


            }

            if (!angular.isDefined(viewParams.showCreateDefaultButton)) {
                viewParams.showCreateDefaultButton = false;
            }

            if (!viewParams.idField) {
                viewParams.idField = 'id';
            }

            if (!viewParams.idTitle) {
                viewParams.idTitle = 'Id';
            }

            if (!viewParams.idLink) {
                viewParams.idLink = viewParams.idField;
            }


            if (viewParams.restService.delete) {
                viewParams.deleteSimpleEntry = function (data) {
                    viewParams.restService.delete({ singleId: data.id}).then(function () {

                        Dialogs.showOKDialog('Datensatz gelöscht', 'Der Datensatz wurde erfolgreich entfernt.');
                        $location.path($location.path() + '/' + data.metaData.type.toLowerCase());
                    });
                };
            }

            if (!viewParams.restServiceFuncGet) {
                viewParams.restServiceFuncGet = viewParams.restService.get;
            }

            viewParams.createSimpleEntry = function () {
                viewParams.restService.post(function (value) {
                    if (value.info.statusCode === 100) {
                        Dialogs.showOKDialog('Datensatz erfolgreich angelegt', 'Der Datensatz wurde erfolgreich angelegt');

                        $location.path($location.path() + '/' + value.result[0].id);
                    }
                    else {
                        Dialogs.showOKDialog('Fehler', 'Fehler ' + value.info.status + ' (' + value.info.statusCode + ') - Etwas ist schief gegangen. Datensatz konnte nicht gespeichert werden');
                    }
                });

            };

            viewParams.deleteSimpleEntry = function (d) {


                Dialogs.showYESNODialog(d.metaData.type + ' ' + d.name + ' löschen?', 'Sind Sie sicher dass Sie diesen Datensatz löschen möchten?', function (yes) {
                        if (yes) {
                            viewParams.restService.delete({singleId: d[viewParams.idField]});
                            viewParams.data.splice(viewParams.data.indexOf(d), 1);
                            Dialogs.showOKDialog('Datensatz ' + d.name + ' wurde gelöscht');
                        }


                    }
                )
                ;
            };

            return viewParams;
        };


        this.requestDetailPageData = function (detailViewParams, $scope) {

            // INFO: rename "id" to "singleId" due to inflictions with listview filter requests
            var params = { singleId: detailViewParams.id};

            detailViewParams.restServiceDetailsFuncGet(params).$promise.then(
                //success
                function (value) {
                    detailViewParams.viewSecondTitle = value.result[0][detailViewParams.viewTitleField];
                    $scope.data = value.result[0];
                    $scope.typeof = function (value) {
                        return typeof(value);
                    };
                    $scope.saveSimpleEntry = function (data) {
                        detailViewParams.restService.post(params, data).$promise.then(function (value) {
                            if (value.info.statusCode === 100) {
                                Dialogs.showOKDialog('Datensatz erfolgreich geändert', 'Der Datensatz wurde erfolgreich geändert', function () {
                                    if (angular.isDefined(detailViewParams.onSavedSuccessfully)) {
                                        detailViewParams.onSavedSuccessfully(data);
                                    }
                                    if (value.result[0]) {
                                        $scope.data = value.result[0];
                                    }

                                });
                            }
                            else {
                                Dialogs.showOKDialog('Fehler', 'Fehler ' + value.info.status + ' (' + value.info.statusCode + ') - Etwas ist schief gegangen. Datensatz konnte nicht gespeichert werden');
                            }
                        }, function (error) {
                            Dialogs.showOKDialog('Fehler', 'Etwas ist schief gegangen. Datensatz konnte nicht gespeichert werden' + error);
                        });
                    };

                    $scope.createDuplicate = function (data) {
                        // unset   id to create a new object
                        delete  data.id;
                        var promise = detailViewParams.restService.post(params, data);

                        promise.$promise.then(function (value) {
                            if (value.info.statusCode === 100) {
                                // Make sure scope object is updated with incoming
                                $scope.data = value.result[0];
                                Dialogs.showOKDialog('Datensatz erfolgreich dupliziert', 'Der Datensatz wurde erfolgreich angelegt', function () {

                                });
                                $location.path('/' + value.result[0].metaData.type.toLowerCase() + '/' + value.result[0].id);
                            }
                            else {
                                Dialogs.showOKDialog('Fehler', 'Fehler Duplizierung ' + value.info.status + ' (' + value.info.statusCode + ') - Etwas ist schief gegangen. Datensatz konnte nicht gespeichert werden');
                            }
                        }, function (error) {
                            Dialogs.showOKDialog('Fehler', 'Etwas ist schief gegangen beim Duplikat anlegen. Datensatz konnte nicht gespeichert werden' + error);
                        });
                    };


                    $scope.deleteSimpleEntry = function (data) {
                        params = {singleId: data.id};
                        Dialogs.showYESNODialog(data.metaData.type + ' ' + data.name + ' löschen?', 'Sind Sie sicher dass Sie diesen Datensatz löschen möchten?', function (yes) {
                                if (yes) {
                                    detailViewParams.restService.delete(params, function () {
                                        Dialogs.showOKDialog('Eintrag gelöscht', 'Datensatz wurde erfolgreich gelöscht');
                                        $location.path('/' + data.metaData.type.toLowerCase());

                                    });
                                }


                            }
                        );

                    };
                    $scope.isImage = function (value) {
                        if (value && value.match(/^((http?|ftp):)?\/\/.*(jpeg|jpg|png|gif)$/)) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    };

                    $scope.isVideo = function (value) {
                        if (value && value.match(/^((http?|ftp):)?\/\/.*(mp4|ogv|webm)$/)) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    };

                    $scope.isText = function (value) {
                        if (value && value.match(/^((http?|ftp):)?\/\/.*(json|xml|txt|wsdl)$/)) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    };
                    $scope.getExtension = function (value) {
                        if ($scope.isImage(value)) {
                            return 'image';
                        } else if ($scope.isVideo(value)) {
                            return 'video';
                        } else {
                            return 'text';
                        }
                    };

                    $scope.loadExternalFile = function (value) {
                        return value;
                    };
                }
            );
        };

        this.createSafeDetailViewParams = function (viewParams) {

            if (!viewParams.restServiceDetailsFuncGet) {
                viewParams.restServiceDetailsFuncGet = viewParams.restService.get;
            }

            return viewParams;
        };

    }
    ])
;
