'use strict';

/**
 * @ngdoc service
 * @name cubeAdminApp.Corporate
 * @description
 * # Corporate
 * Service in the cubeAdminApp.
 */
angular.module('cubeAdminApp')
    .service('Corporate', ['Security',
        function Corporate(Security) {
            var result = Security.getSimpleProtectedAdminResource('Corporate');
            console
                .log('Corporate Service Created', result);

            return result;

        }
    ]).service('CorporateMethods', ['Security', '$resource', 'Helper', 'Dialogs', '$route',
        function Corporate(Security, $resource, Helper, Dialogs, $route) {
            var result = $resource(Helper.getApi() + '/Admin/' + Security.getToken() + '/Corporate/:singleId/:method', {
                singleId: '@singleId',
                method: '@method'
            }, {

                'post': {
                    method: 'POST',
                    isArray: false
                }
            });
            console.log('Corporate Methods Service Created', result);

            result.importDefaultCorporateTemplates = function (corporateData) {
                result.post({'singleId': corporateData.id, 'method': 'importdefaulttemplates'}, function (value) {
                    if (value.info.statusCode === 100) {
                        Dialogs.showOKDialog('Default Templates für Corporate erfolgreich angelegt', '');
                        $route.reload();
                    }
                    else {
                        Dialogs.showOKDialog('Fehler', 'Fehler ' + value.info.status + ' (' + value.info.statusCode + ') - Etwas ist schief gegangen. Default Templates konnten nicht für Corporate angelegt werden');
                    }
                });
            };

            result.importDefaultTemplates = function () {
                result.post({'singleId': 'DEFAULT', 'method': 'importdefaulttemplates'}, function (value) {


                    if (value.info.statusCode === 100) {
                        Dialogs.showOKDialog('Default Templates erfolgreich angelegt', '');
                        $route.reload();
                    }
                    else {
                        Dialogs.showOKDialog('Fehler', 'Fehler ' + value.info.status + ' (' + value.info.statusCode + ') - Etwas ist schief gegangen. Default Templates konnten nicht angelegt werden');
                    }

                });
            };


            return result;

        }
    ]);


