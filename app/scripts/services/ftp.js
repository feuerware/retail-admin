'use strict';

angular.module('cubeAdminApp')
    .service('Ftp', ['Security',
        function Ftp(Security) {
            return Security.getSimpleProtectedAdminResource('FTP');
        }
    ]);

