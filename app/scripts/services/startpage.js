'use strict';

angular.module('cubeAdminApp')
    .service('StartPage', ['Security', '$q' ,
        function StartPage(Security, $q) {
            var result = Security.getSimpleProtectedAdminResource('StartPage');


            // Utility for typeahead search retailer by name
            result.getStartPagesForCorporate = function (corporateId) {
                var deffered = $q.defer();

                result.get({corporateId: corporateId}, function (result) {
                    var results = result.result[0].list || [];
                    deffered.resolve(results);
                    return results;
                });

                return deffered.promise;

            };


            return result;

        }
    ]);
