'use strict';

angular.module('cubeAdminApp')
    .service('MockProducts', function MockProducts() {
        // AngularJS will instantiate a singleton by calling 'new' on this function


        this.addStubs = function ($httpBackend) {
            var products = {
                'info': {
                    'durationMilliSeconds': 14,
                    'status': 'OK',
                    'statusCode': 100
                },
                'result': [
                    {
                        'listHeader': {
                            'listSize': 4647,
                            'paging': {
                                'offset': 0,
                                'pageSize': 1,
                                'pageNumber': 0
                            }
                        },
                        'list': [
                            {
                                'metaData': {
                                    'resource': {
                                        'rel': 'Product',
                                        'href': 'http://92.50.83.218:8100/api/Product/200093'
                                    },
                                    'type': 'Product',
                                    'lastChangedTimestamp': '2014-02-26T10:11:37.463+01:00'
                                },
                                'description': 'Herkunftsland: Italien  HKL.I',
                                'productCategories': [
                                    '108682906'
                                ],
                                'packingUnit': 'Stück',
                                'available': true,
                                'standardProductAmount': 0,
                                'standardProductUnit': 'g',
                                'possibleOrderType': 'PIECE_OR_AMOUNT',
                                'ean': '',
                                'externalProductIdentifier': '51483320',
                                'offer': false,
                                'recommendation': false,
                                'retailerID': '101753124',
                                'id': '200093',
                                'pictures': [
                                    {
                                        'name': '51483320_SMALL.jpg',
                                        'pictureID': '700038066',
                                        'fileSize': 6052,
                                        'fileDate': {
                                            'year': 2010,
                                            'era': 1,
                                            'dayOfYear': 68,
                                            'dayOfWeek': 2,
                                            'dayOfMonth': 9,
                                            'centuryOfEra': 20,
                                            'yearOfEra': 2010,
                                            'yearOfCentury': 10,
                                            'weekyear': 2010,
                                            'monthOfYear': 3,
                                            'weekOfWeekyear': 10,
                                            'hourOfDay': 11,
                                            'minuteOfHour': 59,
                                            'secondOfMinute': 1,
                                            'millisOfSecond': 0,
                                            'millisOfDay': 43141000,
                                            'secondOfDay': 43141,
                                            'minuteOfDay': 719,
                                            'zone': {
                                                'fixed': false,
                                                'uncachedZone': {
                                                    'cachable': true,
                                                    'fixed': false,
                                                    'id': 'Europe/Berlin'
                                                },
                                                'id': 'Europe/Berlin'
                                            },
                                            'millis': 1268132341000,
                                            'chronology': {
                                                'zone': {
                                                    'fixed': false,
                                                    'uncachedZone': {
                                                        'cachable': true,
                                                        'fixed': false,
                                                        'id': 'Europe/Berlin'
                                                    },
                                                    'id': 'Europe/Berlin'
                                                }
                                            },
                                            'afterNow': false,
                                            'beforeNow': true,
                                            'equalNow': false
                                        },
                                        'imageUrlStatic': 'http://appcube-demo.com/tsystems/retail/environmentstest/image_700038066.jpg',
                                        'fileName': 'image_700038066.jpg'
                                    },
                                    {
                                        'name': '51483320_MEDIUM.jpg',
                                        'pictureID': '700038067',
                                        'fileSize': 36775,
                                        'fileDate': {
                                            'year': 2010,
                                            'era': 1,
                                            'dayOfYear': 68,
                                            'dayOfWeek': 2,
                                            'dayOfMonth': 9,
                                            'centuryOfEra': 20,
                                            'yearOfEra': 2010,
                                            'yearOfCentury': 10,
                                            'weekyear': 2010,
                                            'monthOfYear': 3,
                                            'weekOfWeekyear': 10,
                                            'hourOfDay': 11,
                                            'minuteOfHour': 58,
                                            'secondOfMinute': 55,
                                            'millisOfSecond': 0,
                                            'millisOfDay': 43135000,
                                            'secondOfDay': 43135,
                                            'minuteOfDay': 718,
                                            'zone': {
                                                'fixed': false,
                                                'uncachedZone': {
                                                    'cachable': true,
                                                    'fixed': false,
                                                    'id': 'Europe/Berlin'
                                                },
                                                'id': 'Europe/Berlin'
                                            },
                                            'millis': 1268132335000,
                                            'chronology': {
                                                'zone': {
                                                    'fixed': false,
                                                    'uncachedZone': {
                                                        'cachable': true,
                                                        'fixed': false,
                                                        'id': 'Europe/Berlin'
                                                    },
                                                    'id': 'Europe/Berlin'
                                                }
                                            },
                                            'afterNow': false,
                                            'beforeNow': true,
                                            'equalNow': false
                                        },
                                        'imageUrlStatic': 'http://appcube-demo.com/tsystems/retail/environmentstest/image_700038067.jpg',
                                        'fileName': 'image_700038067.jpg'
                                    },
                                    {
                                        'name': '51483320_LARGE.jpg',
                                        'pictureID': '700038068',
                                        'fileSize': 205758,
                                        'fileDate': {
                                            'year': 2010,
                                            'era': 1,
                                            'dayOfYear': 68,
                                            'dayOfWeek': 2,
                                            'dayOfMonth': 9,
                                            'centuryOfEra': 20,
                                            'yearOfEra': 2010,
                                            'yearOfCentury': 10,
                                            'weekyear': 2010,
                                            'monthOfYear': 3,
                                            'weekOfWeekyear': 10,
                                            'hourOfDay': 11,
                                            'minuteOfHour': 52,
                                            'secondOfMinute': 52,
                                            'millisOfSecond': 0,
                                            'millisOfDay': 42772000,
                                            'secondOfDay': 42772,
                                            'minuteOfDay': 712,
                                            'zone': {
                                                'fixed': false,
                                                'uncachedZone': {
                                                    'cachable': true,
                                                    'fixed': false,
                                                    'id': 'Europe/Berlin'
                                                },
                                                'id': 'Europe/Berlin'
                                            },
                                            'millis': 1268131972000,
                                            'chronology': {
                                                'zone': {
                                                    'fixed': false,
                                                    'uncachedZone': {
                                                        'cachable': true,
                                                        'fixed': false,
                                                        'id': 'Europe/Berlin'
                                                    },
                                                    'id': 'Europe/Berlin'
                                                }
                                            },
                                            'afterNow': false,
                                            'beforeNow': true,
                                            'equalNow': false
                                        },
                                        'imageUrlStatic': 'http://appcube-demo.com/tsystems/retail/environmentstest/image_700038068.jpg',
                                        'fileName': 'image_700038068.jpg'
                                    }
                                ],
                                'name': 'MockedRaddicio',
                                'price': [
                                    {
                                        'name': 'Preis',
                                        'priceType': 'PRICE',
                                        'taxPercent': 7,
                                        'centPriceNetto': 42,
                                        'centPriceBrutto': 45,
                                        'centPriceTax': 3
                                    },
                                    {
                                        'name': 'GenauerkgBruttoPreis',
                                        'priceType': 'UNDEFINED',
                                        'taxPercent': 0,
                                        'centPriceNetto': 449,
                                        'centPriceBrutto': 449,
                                        'centPriceTax': 0
                                    }
                                ]
                            }
                        ]
                    }
                ]
            };
            $httpBackend.whenGET(/\/api\/Product$/).respond(products);

            console.log('Injected product mocks');
        };
    });