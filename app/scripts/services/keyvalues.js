'use strict';

angular.module('cubeAdminApp')
    .service('KeyValues', ['Security',
        function KeyValues(Security) {
            return Security.getSimpleProtectedAdminResource('KeyValueGroup');
        }
    ]);

