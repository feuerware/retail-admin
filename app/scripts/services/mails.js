'use strict';

angular.module('cubeAdminApp')
    .service('Mails', ['Security',
        function Mails(Security) {
            return Security.getSimpleProtectedAdminResource('QueueEmail');
        }
    ]);