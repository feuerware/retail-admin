'use strict';
// TODO: FIXME: make use of single call promise instead of stupid caching ...
angular.module('cubeAdminApp')
    .service('Globals', ['Security',
        function Globals(Security) {

            var result = Security.getSimpleProtectedAdminResource('Globals');

                // Rename original get function
                result.globalspromise = result.get({}).$promise;

                // REMOVE GET METHOD FROM RESOURCE; JUST TO AVOID HAVING IT USED SOMEWHERE; SHOULD THROUW AN ERROR
                delete result.get;

            return result;
        }
    ])
;
