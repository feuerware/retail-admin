'use strict';

angular.module('cubeAdminApp')
    .service('User', ['Security',
        function User(Security) {
            return Security.getSimpleProtectedAdminResource('User');
        }
    ]);