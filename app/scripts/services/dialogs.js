'use strict';

angular.module('cubeAdminApp')
    .service('Dialogs', ['$modal', function Dialogs($modal) {

        this.showErrorDialogForResponse = function (value) {
            this.showOKDialog('Ein Fehler ist aufgetreten', JSON.stringify(value));
        };

        this.showOKDialog = function (title, message, callbackFn) {
            var ModalInstanceCtrl = function ($scope, $modalInstance) {

                $scope.message = message;
                $scope.title = title;
                $scope.buttons = [
                    {title: 'ok', method: function () {
                        $modalInstance.dismiss('ok');
                        if (callbackFn) {
                            callbackFn(true);
                        }
                    }}
                ];
            };

            // ShowDialog
            $modal.open({
                templateUrl: 'views/simpledialog.html',
                controller: ModalInstanceCtrl
            });
        };


        this.showYESNODialog = function (title, message, callbackFn) {
            var ModalInstanceCtrl = function ($scope, $modalInstance) {

                $scope.message = message;
                $scope.title = title;

                $scope.ok = function () {
                    $modalInstance.dismiss('ok');
                    if (callbackFn) {
                        callbackFn(true);
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                    if (callbackFn) {
                        callbackFn(false);
                    }
                };

                $scope.buttons = [

                    {title: 'NEIN', method: function () {
                        $modalInstance.dismiss('cancel');
                        if (callbackFn) {
                            callbackFn(false);
                        }
                    }},
                    {title: 'JA', method: function () {
                        $modalInstance.dismiss('ok');
                        if (callbackFn) {
                            callbackFn(true);
                        }
                    }}
                ];
            };

            // Show Dialog
            $modal.open({
                templateUrl: 'views/simpledialog.html',
                controller: ModalInstanceCtrl
            });

        };


        return this;
    }]);
