'use strict';

/**
 * @ngdoc service
 * @name cubeAdminApp.Messages
 * @description
 * # Messages
 * Service in the cubeAdminApp.
 */
angular.module('cubeAdminApp')
    .service('Messages', function () {


        /*

         the messages are filled by the services that needs them ;)
         message format is

         [resource]
         {
         title: the title of the yes/no dialog
         message: the message to display
         success: the message to display on success
         fail: and the message to display on fail
         }

         Template Syntax:

         TODO:  every parameter that is marked with {{}} will be replacd from the input data .... use angular engine for that!


         */

        this.userMessages = {};

        /**
         * the getuser message is called by a resource, with its name and the request data a lookup is performed in the available messages, otherwise a default message is created
         * @param resourceName
         * @param data
         * @returns {*}
         */
        this.getUserMessage = function (resourceName, data) {

            var result = this.userMessages[resourceName][data.singleId];
            if (!result) {
                result = {};
            }

            if (!result.title) {
                result.title = data.singleId;
            }
            if (!result.fail) {
                result.fail = data.singleId + ' Konnte nicht durcheführt werden.';
            }
            if (!result.success) {
                result.success = data.singleId + ' Erfolgreich durchgeführt.';
            }
            if (!result.message) {
                result.message = 'Möchten Sie dass wirklich durchführen?';
            }

            return result;

        };

    });
