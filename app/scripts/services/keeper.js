'use strict';

/**
 * @ngdoc service
 * @name cubeAdminApp.keeper
 * @description
 * # keeper
 * Service in the cubeAdminApp.
 */
angular.module('cubeAdminApp')
    .service('Keeper', ['Security',
        function Keeper(Security) {
            return Security.getSimpleProtectedAdminResource('Keeper');
        }
    ]);