'use strict';

angular.module('cubeAdminApp')
    .service('Note', ['Security',
        function Note(Security) {
            return Security.getSimpleProtectedAdminResource('Note');
        }
    ]);