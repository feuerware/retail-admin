'use strict';

angular.module('cubeAdminApp')
    .service('AppDevice', ['Security',
        function AppDevice(Security) {
            return Security.getSimpleProtectedAdminResource('AppDevice');
        }
    ]).service('AppDevicePushSender', ['$resource', 'Helper', 'Security',
        function AppDevice($resource, Helper, Security) {
            return $resource(Helper.getApi() + '/Admin/' + Security.getToken() + '/AppDevice/:id/test', {
                id: '@id'
            }, {
                'post': {
                    method: 'POST',
                    isArray: false
                }
            });
        }
    ]);