'use strict';

/**
 * @ngdoc service
 * @name cubeAdminApp.ErrorTranslator
 * @description
 * # ErrorTranslator
 * Service in the cubeAdminApp.
 */
angular.module('cubeAdminApp')
    .service('ErrorTranslator', function () {
        // AngularJS will instantiate a singleton by calling "new" on this function

        this.serverErrors = {};
        /*
         OK(100),
         FATAL_ERROR(-1),
         ERROR_HMAC_VERIFICATION_FAILURE(-10),
         ERROR_MONGODB_NETWORK(1001),
         ERROR_MONGODB_CURSORNOTFOUND(1002),
         ERROR_MONGODB_DUPLICATEKEY(1003),
         ERROR_RESOURCE_NOTAVAILABLE(1100),
         ERROR_USER_CHANGE_PASSWORD_INVALID_INPUT(5500),
         ERROR_CREATE_USER_INVALID_INPUT(5000),
         ERROR_CREATE_USER_ALREADY_EXISTANT(5001),
         ERROR_CREATE_USER(5002),
         ERROR_UPDATE_USER_INVALID_INPUT(5100),
         ERROR_LOGIN_USER_PASSWORD_NAME_NOT_VALID(5500),
         ERROR_USER_REQUIRES_ACTIVATION(5501),
         ERROR_USER_IS_BLOCKED(5502),
         //ERROR_TOKEN_EXPIRED(5003),
         ERROR_TOKEN_INVALID(5004),
         ERROR_TOKEN_INVALID_ROLE(5005),
         ERROR_CORPORATEID_NOT_SET(5010),

         ERROR_CREATE_BASKET_INVALID_BASKET(6000),
         ERROR_BASKETITEM_NOTEXISTANT(7000),
         ERROR_CREATE_NOTE(8000),
         ERROR_CREATE_RECOMMENDATION(9000),
         ERROR_CREATE_ORDER(10000),
         ERROR_CREATE_ORDER_INVALID_ITEM(10001),
         ERROR_CREATE_ORDER_MINIMUMBASKETRICE_NOT_MET(10002),
         ERROR_APPVERSION_NOTSUPPORTED(11000),
         ERROR_NOT_YET_IMPLEMENTED(666);

         */

        this.serverErrors['-1'].message = 'Ein Fataler unbekannter Fehler ist aufgetreten.';
        this.serverErrors['-10'].message = 'Transfer HMAC Validierung ungültig. Die Session ist abgelaufen';
        this.serverErrors['1001'].message = 'Ein Mongo Datenbankverbindungsproblem ist aufgetreten.';
        this.serverErrors['1002'].message = 'Ein Mongo Datenbankfehler ist aufgetreten: Cursor nicht gefunden.';
        this.serverErrors['1003'].message = 'Ein Mongo Datenbankfehler ist aufgetreten: Ein eindeutiger Schlüssel mit diesem eintrag ist schon vorhanden.';
        this.serverErrors['5004'].message = 'API Fehler, Eintrag nicht gefunden. Dieser Eintrag existiert nicht im System.';
        this.serverErrors['5005'].message = 'Invalides Benutzer Token. Rolle des Benutzers erlaubt aktion nicht.';
        this.serverErrors['5010'].message = 'Die Corporate ID ist nicht im Header mitgeschickt wurden, oder es ist eine ungültige Corporate ID.';


    });
