'use strict';

angular.module('cubeAdminApp')
    .service('MockUser', function MockUser() {
        // AngularJS will instantiate a singleton by calling 'new' on this function

        this.addStubs = function ($httpBackend) {
            var entries = {
                'info': {
                    'durationMilliSeconds': 98,
                    'status': 'OK',
                    'statusCode': 100
                },
                'result': [
                    {
                        'listHeader': {
                            'listSize': 4647,
                            'paging': {
                                'offset': 0,
                                'pageSize': 1,
                                'pageNumber': 0
                            }
                        },
                        'list': [
                            {
                                'metaData': {
                                    'resource': {
                                        'rel': 'User',
                                        'href': 'http://localhost:8080/api/User/28da8ad8-99f0-45b1-ae04-20183cb8e974'
                                    },
                                    'type': 'User',
                                    'lastChangedTimestamp': '2014-03-05T11:28:35.806+01:00'
                                },
                                'email': 'foofoo@byom.de',
                                'password': '$2a$10$BvGJEtWxUrXd0HzB8fmsh.l5QsxruKGVIYoVg.t03lJDGyTv0DLW6',
                                'address': {
                                    'country': 'Deutschland',
                                    'street': 'Musterstrasse',
                                    'postalCode': '98765',
                                    'houseNumber': '23',
                                    'city': 'Musterstadt'
                                },
                                'salutation': 'FEMALE',
                                'firstName': 'Alfred',
                                'lastName': 'Mustermann',
                                'favoritePaymentType': 'PICKUP_ECCARD',
                                'mobilePhoneNumber': '+49491234567890',
                                'flags': {
                                    'privacyAcceptance': true,
                                    'termsAndConditions': true,
                                    'marketingContact': true
                                },
                                'resources': [
                                    {
                                        'resources': [
                                            {
                                                'rel': 'Basket',
                                                'href': 'User/28da8ad8-99f0-45b1-ae04-20183cb8e974/Basket'
                                            },
                                            {
                                                'rel': 'Note',
                                                'href': 'User/28da8ad8-99f0-45b1-ae04-20183cb8e974/Note'
                                            },
                                            {
                                                'rel': 'Suggestion',
                                                'href': 'User/28da8ad8-99f0-45b1-ae04-20183cb8e974/Suggestion'
                                            },
                                            {
                                                'rel': 'Order',
                                                'href': 'User/28da8ad8-99f0-45b1-ae04-20183cb8e974/Order'
                                            }
                                        ]
                                    }
                                ],
                                'id': '28da8ad8-99f0-45b1-ae04-20183cb8e974'
                            }
                        ]
                    }
                ]};


            var entryDetails = {
                'info': {
                    'durationMilliSeconds': 98,
                    'status': 'OK',
                    'statusCode': 100
                },
                'result': [
                    {
                        'metaData': {
                            'resource': {
                                'rel': 'User',
                                'href': 'http://localhost:8080/api/User/28da8ad8-99f0-45b1-ae04-20183cb8e974'
                            },
                            'type': 'User',
                            'lastChangedTimestamp': '2014-03-05T11:28:35.806+01:00'
                        },
                        'email': 'foofoo@byom.de',
                        'password': '$2a$10$BvGJEtWxUrXd0HzB8fmsh.l5QsxruKGVIYoVg.t03lJDGyTv0DLW6',
                        'address': {
                            'country': 'Deutschland',
                            'street': 'Musterstrasse',
                            'postalCode': '98765',
                            'houseNumber': '23',
                            'city': 'Musterstadt'
                        },
                        'salutation': 'FEMALE',
                        'firstName': 'Alfred',
                        'lastName': 'Mustermann',
                        'favoritePaymentType': 'PICKUP_ECCARD',
                        'mobilePhoneNumber': '+49491234567890',
                        'flags': {
                            'privacyAcceptance': true,
                            'termsAndConditions': true,
                            'marketingContact': true
                        },
                        'resources': [
                            {
                                'resources': [
                                    {
                                        'rel': 'Basket',
                                        'href': 'User/28da8ad8-99f0-45b1-ae04-20183cb8e974/Basket'
                                    },
                                    {
                                        'rel': 'Note',
                                        'href': 'User/28da8ad8-99f0-45b1-ae04-20183cb8e974/Note'
                                    },
                                    {
                                        'rel': 'Suggestion',
                                        'href': 'User/28da8ad8-99f0-45b1-ae04-20183cb8e974/Suggestion'
                                    },
                                    {
                                        'rel': 'Order',
                                        'href': 'User/28da8ad8-99f0-45b1-ae04-20183cb8e974/Order'
                                    }
                                ]
                            }
                        ],
                        'id': '28da8ad8-99f0-45b1-ae04-20183cb8e974'
                    }
                ]
            };


            var loggedIn = {'info': {'durationMilliSeconds': 84, 'status': 'OK', 'statusCode': 100}, 'result': [
                {'metaData': {'resource': {'rel': 'User', 'href': 'http://92.50.83.218:8100/api/User/48e641a1-d0f5-4365-8f69-9b1b448dd3a7'}, 'type': 'User', 'lastChangedTimestamp': '2014-02-25T19:54:12.634+01:00'}, 'email': 'thurn@appcube.info', 'password': '$2a$10$tZJoKJojV5Eomo6Mbn8rF.qaptC7Cjlw.19Gc887MTx.tNjF/A0KC', 'address': {'country': 'Deutschland', 'street': 'Musterstrasse', 'postalCode': '98765', 'houseNumber': '23', 'city': 'Musterstadt'}, 'title': 'Herr', 'firstName': 'Alfred', 'lastName': 'Mustermann', 'favoritePickupStation': {'metaData': {'resource': {'rel': 'PickupStation', 'href': 'http://92.50.83.218:8100/api/PickupStation/703131844'}, 'type': 'PickupStation', 'lastChangedTimestamp': '2014-03-04T16:08:08.170+01:00'}, 'name': 'BNLW', 'pickupTime': {'openTime': {'hour': 13, 'minute': 0}, 'closeTime': {'hour': 14, 'minute': 0}}, 'active': true, 'buildingAddress': {'address': {'location': {'lat': 50.722446, 'lon': 7.144182}, 'street': 'Landgrabenweg', 'postalCode': '53227', 'houseNumber': '151', 'city': 'Bonn'}}, 'id': '703131844'}, 'favoritePaymentType': 'PICKUP_ECCARD', 'mobilePhoneNumber': '1234567890', 'resources': [
                    {'resources': [
                        {'rel': 'Basket', 'href': 'User/48e641a1-d0f5-4365-8f69-9b1b448dd3a7/Basket'},
                        {'rel': 'Note', 'href': 'User/48e641a1-d0f5-4365-8f69-9b1b448dd3a7/Note'},
                        {'rel': 'Suggestion', 'href': 'User/48e641a1-d0f5-4365-8f69-9b1b448dd3a7/Suggestion'},
                        {'rel': 'Order', 'href': 'User/48e641a1-d0f5-4365-8f69-9b1b448dd3a7/Order'}
                    ]}
                ], 'id': '48e641a1-d0f5-4365-8f69-9b1b448dd3a7'}
            ]};


            $httpBackend.whenGET(/api\/Admin\/User($|\?)/).respond(entries);
            $httpBackend.whenGET(/api\/Admin\/User\/\w+.*/).respond(entryDetails);
            $httpBackend.whenGET(/api\/User\/\w+.*$/).respond(loggedIn);

            console.log('Injected user mocks');

        };
    });