'use strict';

angular.module('cubeAdminApp')
    .service('Product', ['Security', 'Globals',
        function Product(Security, Globals) {
            var result = Security.getSimpleProtectedAdminResource('Product');

            // Chain result and replace image url statics in resultproduct

            result.originalGet = result.get;

            // Overwrite original get promise
            result.get = function (params, callback) {


                return result.originalGet(params, function (value) {
                    Globals.globalspromise.then(function (resultglobals) {
                        // Set Image pathes on result
                        var setImageUrlInProductFunction = function (resultglobals, product) {
                            for (var indexPic in product.pictures) {
                                var p = product.pictures[indexPic];
                                p.imageUrlStatic = resultglobals.result[0].imageBaseUrl + p.fileName;
                            }
                        };

                        if (value.result[0].list) {
                            for (var index in value.result[0].list) {
                                var product = value.result[0].list[index];

                                setImageUrlInProductFunction(resultglobals, product);
                            }
                        } else {
                            // single objects contain full path already, dont do anything with result

                        }

                        if (callback) {
                            callback(value);
                        }
                    });

                });
            };


            return result;
        }
    ]);
