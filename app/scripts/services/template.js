'use strict';

angular.module('cubeAdminApp')
    .service('Page', ['Security', '$q',
        function Page(Security, $q) {
            var result = Security.getSimpleProtectedAdminResource('Template');


            // Utility for typeahead search retailer by name
            result.getTemplatesForCorporate = function (corporateId) {
                var deffered = $q.defer();

                result.get({corporateId: corporateId}, function (result) {
                    var results = result.result[0].list || [];
                    deffered.resolve(results);
                    return results;
                });

                return deffered.promise;

            };

            return result;
        }
    ]);