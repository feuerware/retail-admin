'use strict';

/**
 * @ngdoc service
 * @name cubeAdminApp.Logsession
 * @description
 * # Logsession
 * Service in the cubeAdminApp.
 */
angular.module('cubeAdminApp')
    .service('LogSession', ['Security',
        function LogSession(Security) {
            return Security.getSimpleProtectedAdminResource('LogSession');
        }
    ]);