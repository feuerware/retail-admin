'use strict';

angular.module('cubeAdminApp')
    .service('LiveLog', ['Security',
        function LiveLog(Security) {
            return Security.getSimpleProtectedAdminResource('LiveLog');
        }
    ]);