'use strict';

angular.module('cubeAdminApp')
    .service('Order', ['Security',
        function Order(Security) {
            return Security.getSimpleProtectedAdminResource('Order');
        }
    ]);
