'use strict';

angular.module('cubeAdminApp')
    .service('Workflow', ['$resource', 'Helper', 'Security',
        function Workflow($resource, Helper, Security) {
            return $resource(Helper.getApi() + '/Admin/' + Security.getToken() + '/WorkflowInstance/:singleId', {
                singleId: '@singleId'
            }, {
                'get': {
                    method: 'GET',
                    isArray: false
                },
                'post': {
                    method: 'POST',
                    isArray: false
                },
                'delete': {
                    method: 'DELETE',
                    isArray: false
                }
            });
        }
    ]);