'use strict';

angular.module('cubeAdminApp')
    .service('MockPickupStation', function MockPickupStation() {
        // AngularJS will instantiate a singleton by calling 'new' on this function

        this.addStubs = function ($httpBackend) {
            var entries = {
                'info': {
                    'durationMilliSeconds': 1,
                    'status': 'OK',
                    'statusCode': 100
                },
                'result': [
                    {
                        'listHeader': {
                            'listSize': 3,
                            'paging': {
                                'offset': 0,
                                'pageSize': 3,
                                'pageNumber': 0
                            }
                        },
                        'list': [
                            {
                                'metaData': {
                                    'resource': {
                                        'rel': 'PickupStation',
                                        'href': 'http://92.50.83.218:8100/api/PickupStation/703131843'
                                    },
                                    'type': 'PickupStation',
                                    'lastChangedTimestamp': '2014-03-04T16:08:08.169+01:00'
                                },
                                'name': 'Service Zentrale',
                                'pickupTime': {
                                    'openTime': {
                                        'hour': 13,
                                        'minute': 0
                                    },
                                    'closeTime': {
                                        'hour': 15,
                                        'minute': 0
                                    }
                                },
                                'active': true,
                                'buildingAddress': {
                                    'address': {
                                        'location': {
                                            'lat': 50.708572,
                                            'lon': 7.128813
                                        },
                                        'street': 'Friedrich-Ebert-Allee',
                                        'postalCode': '53113',
                                        'houseNumber': '140',
                                        'city': 'Bonn'
                                    }
                                },
                                'id': '703131843'
                            },
                            {
                                'metaData': {
                                    'resource': {
                                        'rel': 'PickupStation',
                                        'href': 'http://92.50.83.218:8100/api/PickupStation/703131844'
                                    },
                                    'type': 'PickupStation',
                                    'lastChangedTimestamp': '2014-03-04T16:08:08.170+01:00'
                                },
                                'name': 'BNLW',
                                'pickupTime': {
                                    'openTime': {
                                        'hour': 13,
                                        'minute': 0
                                    },
                                    'closeTime': {
                                        'hour': 14,
                                        'minute': 0
                                    }
                                },
                                'active': true,
                                'buildingAddress': {
                                    'address': {
                                        'location': {
                                            'lat': 50.722446,
                                            'lon': 7.144182
                                        },
                                        'street': 'Landgrabenweg',
                                        'postalCode': '53227',
                                        'houseNumber': '151',
                                        'city': 'Bonn'
                                    }
                                },
                                'id': '703131844'
                            },
                            {
                                'metaData': {
                                    'resource': {
                                        'rel': 'PickupStation',
                                        'href': 'http://92.50.83.218:8100/api/PickupStation/703206147'
                                    },
                                    'type': 'PickupStation',
                                    'lastChangedTimestamp': '2014-03-04T16:08:08.170+01:00'
                                },
                                'name': 'Emil Nolde Straße',
                                'description': 'Feldtest - Abholstation - Wird nur Donnerstags und Freitags im Rahmen der Feldtests beliefert.\r\n\r\nKeine dauerhafte Belieferung!',
                                'pickupTime': {
                                    'openTime': {
                                        'hour': 16,
                                        'minute': 0
                                    },
                                    'closeTime': {
                                        'hour': 16,
                                        'minute': 15
                                    }
                                },
                                'active': true,
                                'buildingAddress': {
                                    'address': {
                                        'location': {
                                            'lat': 0,
                                            'lon': 0
                                        },
                                        'street': 'Emil Nolde Straße',
                                        'postalCode': '53113',
                                        'houseNumber': '7',
                                        'city': 'Bonn'
                                    }
                                },
                                'id': '703206147'
                            }
                        ]
                    }
                ]
            };


            var entryDetails = {
                'info': {
                    'durationMilliSeconds': 1,
                    'status': 'OK',
                    'statusCode': 100
                },
                'result': [
                    {
                        'metaData': {
                            'resource': {
                                'rel': 'PickupStation',
                                'href': 'http://92.50.83.218:8100/api/PickupStation/703131843'
                            },
                            'type': 'PickupStation',
                            'lastChangedTimestamp': '2014-03-04T16:08:08.169+01:00'
                        },
                        'name': 'Mocked Service Zentrale',
                        'pickupTime': {
                            'openTime': {
                                'hour': 13,
                                'minute': 0
                            },
                            'closeTime': {
                                'hour': 15,
                                'minute': 0
                            }
                        },
                        'active': true,
                        'buildingAddress': {
                            'address': {
                                'location': {
                                    'lat': 50.708572,
                                    'lon': 7.128813
                                },
                                'street': 'Friedrich-Ebert-Allee',
                                'postalCode': '53113',
                                'houseNumber': '140',
                                'city': 'Bonn'
                            }
                        },
                        'id': '703131843'
                    }
                ]
            };


            $httpBackend.whenGET(/api\/PickupStation$/).respond(entries);
            $httpBackend.whenGET(/api\/PickupStation\/\w+.*/).respond(entryDetails);

            console.log('Injected pickupstation mocks');

        };
    });