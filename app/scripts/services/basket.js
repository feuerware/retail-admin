'use strict';

angular.module('cubeAdminApp')
    .service('Basket', ['Security',
        function Basket(Security) {
            return Security.getSimpleProtectedAdminResource('Basket');
        }
    ]);