'use strict';

angular.module('cubeAdminApp')
    .service('Notification', ['Security',
        function Notification(Security) {
            return Security.getSimpleProtectedAdminResource('Notification');
        }
    ]);
