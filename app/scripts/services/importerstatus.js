'use strict';

angular.module('cubeAdminApp')
    .service('ImporterStatus', ['Security', 'Dialogs', 'Messages'  ,
        function ImporterStatus(Security, Dialogs, Messages) {
            var result = Security.getSimpleProtectedAdminResourceWithDialogs(Dialogs, Messages, 'Importer');

            // Insert Messages into Messages Service

            var importerMessages = {};
            importerMessages.full = {
                title: 'Importer Starten',
                message: 'Der Importvorgang wird auf dem Import Server gestartet, dies kann ein wenig dauern.Alle Daten die nicht auf "Behalten" gesetzt sind werden überschrieben oder gelöscht, möchten Sie dass wirklich? ',
                success: 'Import vorgang erfolgreich gestartet.',
                fail: 'Import vorgang konnte nicht gestarted werden.'
            };
            importerMessages.expand = {
                title: 'Indizierung Starten',
                message: 'Der Indizierungsvorgang führt statische berechnungen auf dem Bestand aus. Und dient derzeit zu entwicklungszecken.',
                success: 'Indizierungs vorgang erfolgreich gestartet.',
                fail: 'Indizierung konnte nicht gestarted werden.'
            };

            importerMessages.switch = {
                title: 'Import Daten in Live betrieb übernehmen',
                message: 'Durch diese aktion wird eine kopie der Import datenbank auf das Livesystem vorgenommen, auf "Behalten" gesetzte eigenschaften bleiben erhalten.',
                success: 'Übernahme der Import Datenbank in Live betrieb erfolgreich.',
                fail: 'Die Daten der Import datenbank konnten nicht übernommen werden.'
            };

            importerMessages.full = {
                title: 'Import vorgang starten',
                message: 'Der Import vorgang füllt die Import Datenbank mit externen Daten, dies kann einige Zeit in anspruch nehmen, anschliessend wird automatisch ein wechsel auf das Live System vorgenommen. Sie können den Import vorgang mit "Status aktualisieren" überwachen. Möchten Sie dass wirklich?',
                success: 'Import vorgang gestart, das kann einige Minuten dauern.',
                fail: 'Der Import vorgang konnte nicht gestartet werden.'
            };

            importerMessages.dummy = {
                title: 'Dummy Daten erzeugen',
                message: 'Es werden alle Daten Objecte des systems mit dummy daten angelegt um die funktionalität testen zu können. Achtung: Dabei werden alle bestehenden Produktkatalog Informationen gelöscht.',
                success: 'Dummy Daten erzeugung gestartet, in kürze sind sie verfügbar.',
                fail: 'Die Dummy Daten erzeugung konnte nicht gestartet werden.'
            };


            Messages.userMessages.Importer = importerMessages;

            return result;


        }
    ]);