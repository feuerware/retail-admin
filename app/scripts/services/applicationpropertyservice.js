'use strict';

/**
 * @ngdoc service
 * @name cubeAdminApp.propertyservice
 * @description
 * # propertyservice
 * Service in the cubeAdminApp.
 */
angular.module('cubeAdminApp')
    .service('ApplicationProperty', ['Security', '$q', function (Security, $q) {
        var result = Security.getSimpleProtectedAdminResource('ApplicationProperty');


        result.createProperty = function (propertyName, propertyValue) {
            var defered = $q.defer();

            result.post({}, function (value) {

                    // The post method creates an default entry, after succesful creation, insert values and re-post
                    console.log('Property created..', value);

                    var property = value.result[0];
                    property.key = propertyName;
                    property.value = propertyValue;
                    property.singleId = property.id;
                    //And re-post to id with changed values
                    result.post(property, function (value) {

                        console.log('New Property created, ', value);
                        defered.resolve(value.result[0]);
                    });

                }
            );

            return defered.promise;


        };


        return result;
    }]);
