'use strict';

angular.module('cubeAdminApp')
    .service('ProductCategory', ['Security', 'Retailer', '$q', 'Globals', 'Dialogs',
        function ProductCategory(Security, Retailer, $q, Globals) {
            var result = Security.getSimpleProtectedAdminResource('ProductCategory');

            // Chain the GET Resource Request to modify result and set image pathes in picturelist

            // Move original get function to another variable in result object
            result.originalGet = result.get;
            // chaining
            var newget = function (params, callback) {
                // return the promise of the original get
                return result.originalGet(params, function (result) {

                    // assign result to local variables, or empty array
                    var results = result.result[0].list || [];
                    // if single result assign it to this variable
                    var singleResult = result.result[0];

                    // function declaration for in-loop call to set the static image path in list of pictures of category
                    var setImagePathesForCategoryImages = function (resultglobals, productCategory) {
                        for (var indexPic in productCategory.pictures) {
                            var p = productCategory.pictures[indexPic];
                            p.imageUrlStatic = resultglobals.result[0].imageBaseUrl + p.fileName;
                        }
                    };

                    // Use Global  Service to obtain image path (makes additional call to REST-Globals ... :/
                    Globals.globalspromise.then(function (resultglobals) {
                        if (singleResult) {
                            setImagePathesForCategoryImages(resultglobals, singleResult);
                        } else {
                            for (var index in  results) {
                                setImagePathesForCategoryImages(resultglobals, results[index]);
                            }
                        }
                        // call original callback
                        if (callback) {
                            if (typeof callback === 'function') {
                                callback(result);
                            }
                        } else {
                            if (typeof params === 'function') {
                                params(result);
                            }
                        }
                    });


                });


            };
            result.get = newget;

            // Utility for typeahead search productcategory by name
            result.findProductCategory = function (value) {
                var deffered = $q.defer();
                var param = {};

                if (value) {
                    if (typeof value === 'string') {
                        param.name = value;
                    } else {
                        param = value;
                    }
                }


                result.get(param, function (result) {
                    var results;
                    if (result.result[0].list) {
                        results = result.result[0].list;
                    } else {
                        // Single object, wrap into an array
                        results = [result.result[0]];
                    }
                    deffered.resolve(results);

                    return results;
                });

                return deffered.promise;

            };
            result.findProductCategoryForRetailer = function (value, retailer) {
                var deffered = $q.defer();

                result.get({name: value, retailerId: retailer}, function (result) {
                    var results = result.result[0].list || [];
                    deffered.resolve(results);
                    return results;
                });

                return deffered.promise;

            };

            // function for getting a single category
            result.getProductCategory = function (params) {

                // For requesting a single Object, rename "id" to "singleId" due to conflicts with tableview
                // params["singleId"] = params["id"];

                return ProductCategory.get(params, function (value) {
                    var p = value.result[0];

                    /* move to controller ...
                     $scope.$watch('currentRetailer', function (value) {
                     if (angular.isDefined(value) && angular.isDefined(value.id)) {
                     $scope.data.retailerID = value.id;
                     }
                     });
                     */


                    return p;

                });
            };

            return result;

        }
    ]);