'use strict';

angular.module('cubeAdminApp')
    .service('NotificationLive', ['$resource', 'Helper', function NotificationLive($resource, Helper) {
        return $resource(Helper.getApi() + '/notification/:id', {
            id: '@id'
        }, {
            'post': {
                method: 'POST',
                isArray: false
            }
        });
    }]);
