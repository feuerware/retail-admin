'use strict';

angular.module('cubeAdminApp')
    .service('MockServerStatus', function MockServerStatus() {
        // AngularJS will instantiate a singleton by calling 'new' on this function
        this.addStubs = function ($httpBackend) {
            var status = {
                'info': {
                    'durationMilliSeconds': 69406,
                    'status': 'OK',
                    'statusCode': 100
                },
                'result': [
                    {
                        'listHeader': {
                            'listSize': 6,
                            'paging': {
                                'offset': 0,
                                'pageNumber': 0,
                                'pageSize': 6
                            }
                        },
                        'list': [
                            {
                                'isWorking': false,
                                'log': [
                                    'Tested 5 services',
                                    'FTP UploaderService working',
                                    'Email Sender Service working',
                                    'AAX Products Service not working',
                                    'AAX Shop Service working',
                                    'Push Sender Service working'
                                ],
                                'name': 'Total Status'
                            },
                            {
                                'isWorking': true,
                                'log': [
                                    'Host: appcube-demo.com'
                                ],
                                'name': 'FTP UploaderService'
                            },
                            {
                                'isWorking': true,
                                'log': [
                                    'From: order@appcube-demo.com',
                                    'AdminMail: ck@digitalgott.de'
                                ],
                                'name': 'Email Sender Service'
                            },
                            {
                                'isWorking': false,
                                'log': [
                                    'Host: http://77.244.241.136:9338/axis2/services/products',
                                    'Items could not be loaded or are empty'
                                ],
                                'name': 'AAX Products Service'
                            },
                            {
                                'log': [
                                    'Host: http://77.244.241.136:9338/axis2/services/shop',
                                    'Warning: No method known to check if connection works'
                                ],
                                'name': 'AAX Shop Service'
                            },
                            {
                                'isWorking': true,
                                'log': [
                                    'Testing iOS Sandbox and Production Connection',
                                    'APNS Sandbox connection tested succesfully',
                                    'APNS Production connection tested succesfully'
                                ],
                                'name': 'Push Sender Service'
                            }
                        ]
                    }
                ]
            };
            $httpBackend.whenGET(/api\/Admin\/Status$/).respond(status);

            console.log('Injected serverstatus mocks');
        };


    });