'use strict';

angular.module('aInterceptor', [], function ($provide, $httpProvider) {
    $provide.factory('simpleHttpInterceptor', function ($q, $injector, Helper) {
        var runningRequestURIs = [];

        return function (promise) {


            return promise.then(
                function (response) {

                    try {
                        var statusCode = response.data.info.statusCode;
                        if (statusCode === 5004) {


                            var alreadySent = runningRequestURIs.indexOf('shown');//FIXME: response.config.url);
                            if (alreadySent < 0) {
                                runningRequestURIs.push('shown');//FIXME: response.config.url);

                                var Security = $injector.get('Security');
                                if (response.config.url !== Security.getUrlForLogin()) {
                                    console.log('token invalid ' + statusCode + ' ' + response.config.url);

                                    Security.getLocalUser().$promise.then(function (value) {

                                        if (Helper.isResponseOK(value)) {
                                            Security.setToken(value.result[0].id);
                                        }
                                    });
                                    var dialogs = $injector.get('Dialogs');
                                    dialogs.showOKDialog('Session abgelaufen', 'Die Seite wird nach Klick auf OK neu geladen', function () {
                                        // console.log('reload');
                                        window.location.reload();
                                    });
                                    return response;


                                    /*
                                     var $http = $injector.get('$http');

                                     var retryCallBack = $http(response.config).then(function (result) {
                                     var index = runningRequestURIs.indexOf(response.config.url);
                                     runningRequestURIs.splice(index, 1);
                                     return result;
                                     });
                                     var totalPromise = Security.getLocalUser().$promise.then(retryCallBack);

                                     return totalPromise;
                                     */
                                }

                            }

                        }
                        //console.log(statusCode);
                    }
                    catch (e) {

                    }

                    return response;
                }, function (response) {

                    return $q.reject(response);
                });
        };
    });
    $httpProvider.responseInterceptors.push('simpleHttpInterceptor');
});