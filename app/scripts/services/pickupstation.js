'use strict';

angular.module('cubeAdminApp')
    .service('Pickupstation', ['Security', '$q',
        function Pickupstation(Security, $q) {
            var result = Security.getSimpleProtectedAdminResource('PickupStation');

            // Utility for typeahead search pickupstation by name
            result.findPickupStations = function (value) {
                var deffered = $q.defer();

                var param = {};

                if (value) {
                    if (typeof value === 'string') {
                        param.name = value;
                    } else {
                        param = value;
                    }
                }

                result.get(param, function (result) {
                    var results;
                    if (result.result[0].list) {
                        results = result.result[0].list;
                    } else {
                        // Single object, wrap into an array
                        results = [result.result[0]];
                    }
                    deffered.resolve(results);
                    return results;
                });

                return deffered.promise;

            };


            return result;

        }
    ]);