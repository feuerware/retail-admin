'use strict';

angular.module('cubeAdminApp')
    .service('Enums', function Enums() {

        this.getPossibleOrderTypes = function () {
            return [
                {title: 'Stück', value: 'PIECE'},
                {title: 'Menge', value: 'AMOUNT'},
                {title: 'Stück oder Menge', value: 'PIECE_OR_AMOUNT'}
            ];
        };

        this.getPriceTypes = function () {
            return [
                {title: 'Stück', value: 'PIECE'},
                {title: 'Menge', value: 'TAX'},
                {title: 'Preis', value: 'PRICE'},
                {title: 'Lieferkosten', value: 'DELIVERY_COST'},
                {title: 'Pfand', value: 'DEPOSIT'},
                {title: 'Wert', value: 'VALUEOFGOODS'}

            ];
        };

        this.getOrderStates = function () {
            return [
                {title: 'Angenommen', value: 'NEW', code: 0},
                {title: 'In Bearbeitung', value: 'ACTIVE', code: 1},
                {title: 'Abgelehnt', value: 'REFUSED', code: 2},
                {title: 'Gesperrt', value: 'BLOCKED', code: 3},
                {title: 'Deaktiviert', value: 'DEACTIVATED', code: 6},
                {title: 'Gekündigt', value: 'QUITTED', code: 9},
                {title: 'Wird ausgeliefert', value: 'IN_DELIVERY', code: 11},
                {title: 'Storniert', value: 'CANCELLED', code: 13},
                {title: 'Nicht verfügbar', value: 'NOT_AVAILABLE', code: 14},
                {title: 'Gelöscht', value: 'DELETED', code: 17},
                {title: 'Abgelaufen', value: 'EXPIRED', code: 19},
                {title: 'Abholbereit', value: 'AT_PICKUPSTATION', code: 20},
                {title: 'Abgeholt', value: 'DELIVERED', code: 21},
                {title: 'Fehler', value: 'ERROR', code: 100}
            ];

        };

        this.getProductCategoryLayoutHint = function () {
            return [
                {title: 'Veraltet', value: 'TILEPLANESMALL'},
                {title: 'Default', value: 'DEFAULT'},
                {title: 'Kleine Kachel (Bild oder Text)', value: 'TILEPLAINSMALL'},
                {title: 'Breite Kachel (Bild oder Text)', value: 'TILEPLAINWIDE'},
                {title: 'Riesige Kachel (Bild oder Text)', value: 'TILEPLAINLARGE'},
                {title: 'Kleine Kachel (Bild und Text)', value: 'TILEIMAGETEXTSMALL'},
                {title: 'Breite Kachel (Bild und Text)', value: 'TILEIMAGETEXTWIDE'},
                {title: 'Riesige Kachel (Bild und Text)', value: 'TILEIMAGETEXTLARGE'}
            ];
        };

        this.getUserRoles = function () {
            return [
                {title: 'Benutzer', role: 'ROLE_USER'},
                {title: 'Administrator', role: 'ROLE_ADMIN'},
                {title: 'Fahrer', role: 'ROLE_DRIVER'}
            ];
        };

        this.getSalutations = function () {
            return [
                {title: 'Herr', value: 'MALE'},
                {title: 'Frau', value: 'FEMALE'},
                {title: 'Firma', value: 'COMPANY'}
            ];
        };


        this.getNotificationStates = function () {
            return [
                {title: 'Neu', value: 'NEW'},
                {title: 'Verarbeitet', value: 'PROCESSED'},
                {title: 'Fehler', value: 'ERROR'}
            ];
        };


        this.getWorkflowStates = function () {
            return [
                {title: 'Neu', value: 'NEW'},
                {title: 'In Bearbeitung', value: 'PROCESSING'},
                {title: 'Initialisiert', value: 'INITIALISED'},
                {title: 'Activität beginnt', value: 'ACTIVITY_BEGIN'},
                {title: 'Aktivität wird bearbeitet', value: 'ACTIVITY_PROCESSING'},
                {title: 'Aktivität beendet', value: 'ACTIVITY_END'},
                {title: 'Aktivität Fehler', value: 'ACTIVITY_ERROR'},
                {title: 'Abgebrochen', value: 'CANCELLED_ERROR'}
            ];

        };


        this.getTitleByValue = function (array, value) {
            var result = null;
            angular.forEach(array, function (v) {
                if (v.value === value) {
                    result = v.title;
                    return false;
                }
            });
            return result;
        };


        this.getPageVisibilityStates = function () {
            return [
                {title: 'Öffentlich', value: 'PUBLIC'},
                {title: 'Unveröffentlicht', value: 'PRIVATE'}
            ];
        };

        this.getPageTypes = function () {
            return [
                {title: 'HTML', value: 'HTML'},
                {title: 'TEXT', value: 'TEXT'},
                {title: 'JSON', value: 'JSON'},
                {title: 'XML', value: 'XML'},
                {title: 'TEMPLATE', value: 'TEMPLATE'}

            ];
        };

        this.getNotificationStates = function () {
            return [
                {title: 'Neu', value: 'NEW'},
                {title: 'Verarbeitet', value: 'PROCESSED'},
                {title: 'Fehler', value: 'ERROR'}
            ];
        };

        this.getKeeperTargetTypes = function () {
            return [
                {title: 'Produkt', value: 'Product', path: 'product'},
                {title: 'Kategorie', value: 'ExplodedProductCategory', path: 'productcategory'}
            ];
        };

    });
