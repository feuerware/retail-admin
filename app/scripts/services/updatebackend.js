'use strict';

angular.module('cubeAdminApp')
    .service('UpdateBackend', ['Security',
        function UpdateBackend(Security) {
            return Security.getSimpleProtectedAdminResource('UpdateBackend');
        }
    ]);
